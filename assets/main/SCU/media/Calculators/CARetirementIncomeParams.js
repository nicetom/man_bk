


  KJE.parameters.set("AGE_OF_RETIREMENT",65);
  KJE.parameters.set("ANNUAL_CONTRIBUTION",2000);
  KJE.parameters.set("CURRENT_AGE",30);
  KJE.parameters.set("CURRENT_TAX_RATE",KJE.Default.TaxRate);
  KJE.parameters.set("INCREASE_ANNUAL_CONTRIBUTION",false);
  KJE.parameters.set("INFLATION_RATE",KJE.Default.InflationRate);
  KJE.parameters.set("MSG_TAX1","This assumes that all of your investments were made after-tax, and that you paid income taxes on any interest as it was earned.  Any interest earned on your balance at retirement will also be subject to income taxes in the year the interest is earned.");
  KJE.parameters.set("MSG_TAX2","This assumes that all of your investments were before taxes, and that you will pay income taxes on those investments as well as any interest when withdrawals are made.  The remaining balance will still earn tax-deferred interest.")

  KJE.parameters.set("RATE_OF_RETURN",KJE.Default.RORMarket);
  KJE.parameters.set("RETIREMENT_ROR",KJE.Default.RORRetire);
  KJE.parameters.set("RETIREMENT_TAX_RATE",KJE.Default.TaxRateRetire);
  KJE.parameters.set("STARTING_BALANCE",0);
  KJE.parameters.set("TAX_DEFERRED",true);
  KJE.parameters.set("YEARS_OF_RETIREMENT",30);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

