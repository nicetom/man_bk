


  KJE.parameters.set("INTEREST_RATE",KJE.Default.RatePersonal);
  KJE.parameters.set("LOAN_AMOUNT",20000);
  KJE.parameters.set("MONTHLY_PAYMENT",300);
  KJE.parameters.set("PAYMENT_CALC",1);
  KJE.parameters.set("PAYMENT_OPTION",2);
  KJE.parameters.set("PAYMENT_PERCENT_INTEREST",100);
  KJE.parameters.set("PAYMENT_PERCENT_PRINCIPAL1",1);
  KJE.parameters.set("PAYMENT_PERCENT_PRINCIPAL2",1.5);
  KJE.parameters.set("PAYMENT_PERCENT_PRINCIPAL3",2);
  KJE.parameters.set("TERM",48);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

