


  KJE.parameters.set("PURCHASE_PRICE",KJE.Default.HomePrice);
  KJE.parameters.set("PERCENT_DOWNPAYMENT",20);
  KJE.parameters.set("INTEREST_RATE",KJE.Default.RateFix30);
  KJE.parameters.set("PREPAY_AMOUNT",0);
  KJE.parameters.set("PREPAY_STARTS_WITH",1);
  KJE.parameters.set("TERM",25);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

