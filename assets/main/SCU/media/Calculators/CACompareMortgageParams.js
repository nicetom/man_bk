


    KJE.CalculatorTitle = "Mortgage Compare";
    KJE.parameters.set("COMPARE_TYPE","CAMORTGAGE");
    KJE.parameters.getSet("PAYMENT_TYPE1",KJE.Default.PAY_MONTHLY);
    KJE.parameters.getSet("PAYMENT_TYPE2",KJE.Default.PAY_MONTHLY);
    KJE.parameters.getSet("PAYMENT_TYPE3",KJE.Default.PAY_MONTHLY);
    KJE.parameters.set("AMORTIZATION1",15);
    KJE.parameters.set("AMORTIZATION2",20);
    KJE.parameters.set("AMORTIZATION3",25);
    KJE.parameters.set("LOAN_TERM1",5);
    KJE.parameters.set("LOAN_TERM2",5);
    KJE.parameters.set("LOAN_TERM3",5);

    KJE.parameters.set("INTEREST_RATE1",KJE.Default.RateFix15);
    KJE.parameters.set("INTEREST_RATE2",KJE.Default.RateFix30);
    KJE.parameters.set("INTEREST_RATE3",KJE.Default.RateFix30);
    KJE.parameters.set("LOAN_AMOUNT1",100000);
    KJE.parameters.set("LOAN_AMOUNT2",100000);
    KJE.parameters.set("LOAN_AMOUNT3",100000);
    KJE.parameters.set("MSG_LOAN1","Mortgage 1");
    KJE.parameters.set("MSG_LOAN2","Mortgage 2");
    KJE.parameters.set("MSG_LOAN3","Mortgage 3");
    KJE.parameters.set("OTHER_FEES1",0);
    KJE.parameters.set("OTHER_FEES2",0);
    KJE.parameters.set("OTHER_FEES3",0);
    

/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

