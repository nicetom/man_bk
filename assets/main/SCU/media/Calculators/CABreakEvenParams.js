


  KJE.parameters.set("EXPECTED_UNIT_SALES",25000);
  KJE.parameters.set("FIXED_COST",50000);
  KJE.parameters.set("PRICE",5);
  KJE.parameters.set("VARIABLE_UNIT_COST",2.5);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

