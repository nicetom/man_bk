


    KJE.parameters.set("INTEREST_RATE",KJE.Default.RateFix30);
    KJE.parameters.set("LOAN_AMOUNT",KJE.Default.HomePrice);
    KJE.parameters.set("PREPAY_AMOUNT",100);
    KJE.parameters.set("TERM",KJE.Default.MortgageTerm);
    KJE.parameters.set("MORTGAGE_YRS_LEFT",KJE.Default.MortgageTerm-5);
    KJE.parameters.set("PREPAY_TYPE",KJE.Default.PREPAY_MONTHLY);


    

/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

