


  KJE.parameters.set("ESTIMATED_ANNUAL_RRSP_RETURN",KJE.Default.RORMarket);
  KJE.parameters.set("INTEREST_RATE",KJE.Default.RateRRSP);
  KJE.parameters.set("LOAN_AMOUNT",1000);
  KJE.parameters.set("MARGINAL_TAX_RATE",KJE.Default.TaxRate);
  KJE.parameters.set("TAX_REFUNDTO_APPLY_TO_LOAN_BALANCE",100);
  KJE.parameters.set("TERM",2);
  KJE.parameters.set("YEARS_UNTIL_RETIREMENT",10);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

