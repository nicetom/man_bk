


  KJE.parameters.set("ADDITIONAL_CONTRIBUTIONS",50);
  KJE.parameters.set("COMPOUND_INDEX",  KJE.Default.COMPOUND_ANNUALLY);
  KJE.parameters.set("FREQ_INDEX",KJE.Default.SAVE_MONTHLY);
  KJE.parameters.set("MSG_RATE_OF_RETURN1","Annual percentage yield 1");
  KJE.parameters.set("MSG_RATE_OF_RETURN2","Annual percentage yield 2");
  KJE.parameters.set("MSG_RATE_OF_RETURN3","Annual percentage yield 3");
  KJE.parameters.set("RATE_OF_RETURN1",KJE.Default.RORSave);
  KJE.parameters.set("RATE_OF_RETURN2",KJE.Default.RORSave +.5);
  KJE.parameters.set("RATE_OF_RETURN3",KJE.Default.RORSave +1);
  KJE.parameters.set("SIMPLE",true);
  KJE.parameters.set("STARTING_AMOUNT",1000);
  KJE.parameters.set("YEARS",10);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

