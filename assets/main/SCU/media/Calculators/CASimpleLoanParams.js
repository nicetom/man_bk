


  KJE.parameters.set("HIDE_EXTRA_GRAPHS",false);
  KJE.parameters.set("INTEREST_RATE",KJE.Default.RatePersonal);
  KJE.parameters.set("LOAN_AMOUNT",20000);
  KJE.parameters.set("MONTHLY_PAYMENT",300);
  KJE.parameters.set("TERM",60);


/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

