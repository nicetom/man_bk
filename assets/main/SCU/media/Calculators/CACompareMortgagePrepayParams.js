


  KJE.CalculatorTitle = "Mortgage Compare";
  KJE.parameters.set("COMPARE_TYPE","CAMORTGAGE");
  KJE.parameters.getSet("PAYMENT_TYPE1",KJE.Default.PAY_MONTHLY);
  KJE.parameters.getSet("PAYMENT_TYPE2",KJE.Default.PAY_MONTHLY);
  KJE.parameters.getSet("PAYMENT_TYPE3",KJE.Default.PAY_MONTHLY);
  KJE.parameters.set("TERM1",60);
  KJE.parameters.set("TERM2",60);
  KJE.parameters.set("TERM3",60);
  KJE.parameters.set("LOAN_AMORTIZATION1",15);
  KJE.parameters.set("LOAN_AMORTIZATION2",20);
  KJE.parameters.set("LOAN_AMORTIZATION3",25);
  KJE.parameters.set("INTEREST_RATE1",KJE.Default.RateFix15);
  KJE.parameters.set("INTEREST_RATE2",KJE.Default.RateFix30);
  KJE.parameters.set("INTEREST_RATE3",KJE.Default.RateFix30);
  KJE.parameters.set("LOAN_AMOUNT1",100000);
  KJE.parameters.set("LOAN_AMOUNT2",100000);
  KJE.parameters.set("LOAN_AMOUNT3",100000);
  KJE.parameters.set("MSG_LOAN1","Mortgage 1");
  KJE.parameters.set("MSG_LOAN2","Mortgage 2");
  KJE.parameters.set("MSG_LOAN3","Mortgage 3");
  KJE.parameters.set("CALC_INDEX1",KJE.Default.CALC_PAYMENT);
  KJE.parameters.set("CALC_INDEX2",KJE.Default.CALC_PAYMENT);
  KJE.parameters.set("CALC_INDEX3",KJE.Default.CALC_PAYMENT);
  KJE.parameters.set("PREPAY_AMOUNT1",0);
  KJE.parameters.set("PREPAY_AMOUNT2",0);
  KJE.parameters.set("PREPAY_AMOUNT3",0);
  KJE.parameters.set("PREPAY_STARTS_WITH1",1);
  KJE.parameters.set("PREPAY_STARTS_WITH2",1);
  KJE.parameters.set("PREPAY_STARTS_WITH3",1);

  

/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

