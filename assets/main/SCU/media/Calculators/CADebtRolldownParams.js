


KJE.parameters.set("ADDITIONAL_AMOUNT",25);
KJE.parameters.set("CC_BALANCE1",3000);
KJE.parameters.set("CC_RATE1",18.90);
KJE.parameters.set("CC_BALANCE2",4000);
KJE.parameters.set("CC_RATE2",17.50);


/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

