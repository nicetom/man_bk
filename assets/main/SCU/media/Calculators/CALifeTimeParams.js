


  KJE.parameters.set("CURRENT_AGE",30);
  KJE.parameters.set("GENDER",true);  // true is male, false is female
  KJE.parameters.set("BLOOD_PRESSURE",0);   // 0 to 2
  KJE.parameters.set("DRINKING_ALCOHOL",0);  // 0 to  3
  KJE.parameters.set("DRIVING",0);          // 0 to 2
  KJE.parameters.set("FAMILY_HISTORY",0);  // 0 to 4
  KJE.parameters.set("SMOKER",true);
  KJE.parameters.set("HEIGHT",72);
  KJE.parameters.set("WEIGHT",195);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

