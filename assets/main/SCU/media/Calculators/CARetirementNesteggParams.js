


  KJE.parameters.set("AGE_OF_RETIREMENT",65);
  KJE.parameters.set("CURRENT_AGE",45);
  KJE.parameters.set("CURRENT_SAVINGS",100000);
  KJE.parameters.set("HOUSEHOLD_INCOME",50000);
  KJE.parameters.set("INCOME_PERCENT",90);
  KJE.parameters.set("INFLATION_RATE",KJE.Default.InflationRate);
  KJE.parameters.set("POST_RATE_OF_RETURN",KJE.Default.RORRetire);
  KJE.parameters.set("PRE_RATE_OF_RETURN",KJE.Default.RORMarket);
  KJE.parameters.set("SALARY_PERCENT",2);
  KJE.parameters.set("YEARS_OF_RETIREMENT",35);

  

/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

