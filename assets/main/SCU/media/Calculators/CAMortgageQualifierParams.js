


  KJE.parameters.set("ANNUAL_INCOME",80000);
  KJE.parameters.set("MONTHLY_CAR_PAYMENTS",300);
  KJE.parameters.set("MONTHLY_HEAT_AMT",190);
  KJE.parameters.set("OTHER_LOAN_PAYMENTS",0);
  KJE.parameters.set("CREDIT_CARD_PAYMENTS",100);
  KJE.parameters.set("DOWNPAYMENT_CLOSING_CASH",25000);
  KJE.parameters.set("INTEREST_RATE",KJE.Default.RateFix30);
  KJE.parameters.set("INVESTMENT_RETURN",KJE.Default.RORMarket);
  KJE.parameters.set("TERM",KJE.Default.MortgageTerm);
  KJE.parameters.set("LOAN_ORIGINATION_AMT",0);
  KJE.parameters.set("MAINTENANCE",0);
  KJE.parameters.set("MONTHLY_RENT",1750);
  KJE.parameters.set("OTHER_CLOSING_COSTS",800);
  KJE.parameters.set("PRICE_OF_HOME",KJE.Default.HomePrice);
  KJE.parameters.set("PROPERTY_TAX_AMT",1000);

  

/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

