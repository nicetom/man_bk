


  KJE.parameters.set("INITIAL_INVESTMENT_AMOUNT",100000);
  KJE.parameters.set("INTEREST_RATE",5.0);
  KJE.parameters.set("MONTHLY_PAYMENT",1000);
  KJE.parameters.set("PAYMENT_TYPE",0);
  KJE.parameters.set("START_DATE","TODAY");
  KJE.parameters.set("BIRTH_DATE",KJE.RrifCalc.getBirthDateAge65());
  KJE.parameters.set("YEARS_TO_DISPLAY",40);
  KJE.parameters.set("YEARS_TO_WITHDRAW",25);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

