


  KJE.parameters.set("AMT_CURRENT",1000);
  KJE.parameters.set("AMT_SAVE_MONTH",200);
  KJE.parameters.set("FED_TAX_RATE",KJE.Default.TaxRate);
  KJE.parameters.set("INFLATION_RATE",KJE.Default.InflationRate);
  KJE.parameters.set("ROR_INVEST",KJE.Default.RORSave);
  KJE.parameters.set("YEARS_TO_SAVE",10);



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

