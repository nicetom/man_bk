


KJE.parameters.set("CONTRIB_AMOUNT",0);
KJE.parameters.set("CONTRIB_ANNUAL_ROR",KJE.Default.RORMarket);
KJE.parameters.set("CONTRIB_FREQUENCY",6);
KJE.parameters.set("CONTRIB_STARTING_BALANCE",0);
KJE.parameters.set("CONTRIB_TAX_RATE",KJE.Default.TaxRate);
KJE.parameters.set("CONTRIB_YEARS",10);




/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

