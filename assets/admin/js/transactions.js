$(document).ready(function () {
    'use strict';
    var base_url = $('meta[name="base_url"]').attr('content');

    $(".recent").click(function (e) {
        e.preventDefault();
        $('.recent').html('<img src=' + base_url + '/assets/images/ajax-loader.gif />&nbsp; Retrieving Transactions ').prop('disabled', true);
        var account_id = this.id;
        var api = base_url + '/user/transactions/' + account_id;

        getTransactions(api);
    });

    function getTransactions(api_route) {
        $.ajax({
            url: api_route,
            type: 'GET',
            dataType: "json",
            cache: false,

        }).done(function (data) {
            $('.recent').text('Recent Transactions').prop('disabled', false);
            var index = data.from;
            $('#transaction_content').empty();
            var transactions = data.data;
            // $('html, body').stop().animate({
            //     scrollTop: $($('#transaction_content')).offset().top
            // }, 1000, 'linear');
            if (transactions.length != 0) {
                jQuery.each(transactions, function (i, val) {
                    var tr = $('<tr></tr>');

                    var count = $('<td></td>').text(index).appendTo(tr);
                    var date = $('<td></td>').text(val.date_of_transaction).appendTo(tr);
                    var description = $('<td></td>').text(val.description).appendTo(tr);
                    if (val.status_id == '1') {
                        var status = $('<td></td>').html($('<span></span>').addClass('badge badge-success').text('Success')).appendTo(tr);
                    } else if (val.status_id == '2') {
                        var status = $('<td></td>').html($('<span></span>').addClass('badge badge-danger').text('Declined')).appendTo(tr);
                    } else if (val.status_id == '3') {
                        var status = $('<td></td>').html($('<span></span>').addClass('badge badge-warning').text('Processing')).appendTo(tr);
                    }

                    if (val.transaction_type_id == '1') {
                        var type = $('<td></td>').html($('<span></span>').addClass('money-credit').text('Credit')).appendTo(tr);
                    } else if (val.transaction_type_id == '2') {
                        var type = $('<td></td>').html($('<span></span>').addClass('money-debit').text('Debit')).appendTo(tr);
                    }

                    var amount = $('<td></td>').text(val.modified_amount).appendTo(tr);
                    var balance = $('<td></td>').text(val.modified_balance).appendTo(tr);
                    index += 1;
                    $(tr).appendTo('#transaction_content');

                });

                $('#content').empty();
                $('#content').text(data.current_page + ' / ' + data.last_page);
                if (data.prev_page_url == null) {
                    $('#previous').attr('route', '#').prop('disabled', true);
                } else {
                    $('#previous').attr('route', data.prev_page_url);
                }

                if (data.next_page_url == null) {
                    $('#next').attr('route', '#').prop('disabled', true);
                } else {
                    $('#next').attr('route', data.next_page_url);
                }
                $('#pagination').removeClass('hide').addClass('show');

            } else {
                var tr = $('<tr></tr>');
                var td = $('<td></td>').attr('colspan', '10').text('No Recent Transactions').addClass('middle');
                $(td).appendTo(tr);
                $(tr).appendTo('#transaction_content');
            }

        });
    }

    $('#next').click(function (e) {
        var new_route = $('#next').attr('route');
        getTransactions(new_route);
    });

    $('#previous').click(function (e) {
        var new_route = $('#previous').attr('route');
        getTransactions(new_route);
    });



});
