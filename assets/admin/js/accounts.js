$(document).ready(function () {
    'use strict';
    var base_url = $('meta[name="base_url"]').attr('content');

    $("#user_id").change(function (e){
        e.preventDefault();
        $('#submit').html('<img src=' + base_url + '/assets/images/ajax-loader.gif />&nbsp; Retrieving Accounts ').prop('disabled', true);
        var user_id = $('select[name="user_id"]').val();

        $.ajax({
            url: base_url + "/admin/dashboard/accounts/"+user_id,
            type: 'GET',
            dataType: "json",
            cache: false,

        }).done(function (data) {
            $('#submit').html('Submit').prop('disabled', false);
            $('#accounts').empty();
            jQuery.each(data, function(i, val) {
                var option = $('<option></option>');
                $(option).attr('value',val.id).text(val.bank+' - '+val.account_type+' - '+val.account_number+' - '+val.balance);
                $(option).appendTo('#accounts');
            });

        });


    });

});
