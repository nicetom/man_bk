<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    protected $table = "accounts";

    protected $fillable = [
        'account_number','balance','bank_id','account_type_id','user_id'
    ];

    public $timestamps = false;

    public function bank(){
        return $this->hasOne(Banks::class,'id','bank_id');
    }

    public function account_type(){
        return $this->hasOne(AccountType::class,'id','account_type_id');
    }

    public function transactions(){
        return $this->hasMany(Transactions::class,'account_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
