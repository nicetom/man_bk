<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'password','currency_id','status_id','admin_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function image(){
        return $this->hasOne(Images::class,'user_id');
    }

    public function accounts(){
        return $this->hasMany(Accounts::class,'user_id');
    }

    public function currency(){
        return $this->hasOne(Currency::class,'id','currency_id');
    }

    public function status(){
        return $this->hasOne(Status::class,'id','status_id');
    }


}
