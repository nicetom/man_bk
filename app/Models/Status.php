<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "statuses";

    protected $fillable = [
        'name','css_class'
    ];

    public $timestamps = false;
}
