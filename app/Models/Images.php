<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = "profile_images";
    protected $fillable = [
        'user_id','image_location'
    ];
    public $timestamps = false;
}
