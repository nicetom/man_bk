<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pins extends Model
{
    protected $table = "pins";
    protected $fillable = [
        'pin','status'
    ];
    public $timestamps = false;
}
