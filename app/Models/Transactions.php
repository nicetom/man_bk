<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = "transactions";

    protected $fillable = [
        'account_id', 'bank_id', 'status_id', 'transaction_type_id', 'account_name', 'account_number', 'routing_number', 'swift_code', 'description', 'balance', 'amount',
        'date_of_transaction'
    ];

    protected $appends = ['modified_balance','modified_amount'];

    public function getModifiedBalanceAttribute()
    {
        return number_format($this->balance,2,'.',',');
    }

    public function getModifiedAmountAttribute()
    {
        return number_format($this->amount,2,'.',',');
    }

    public $timestamps = false;

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'account_id', 'id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function transaction_type()
    {
        return $this->hasOne(TransactionTypes::class, 'id', 'transaction_type_id');
    }

    public function bank()
    {
        return $this->hasOne(Banks::class, 'id', 'bank_id');
    }
}
