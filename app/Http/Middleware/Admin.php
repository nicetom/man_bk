<?php

namespace App\Http\Middleware;
use Auth;
use Session;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('admin')->check()){
            Session::put('red',1);
            return redirect()->route('admin_login')->withErrors(['message' => 'Please Login to Continue']);
        }
        return $next($request);
    }
}
