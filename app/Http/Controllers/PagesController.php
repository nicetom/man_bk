<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home(Request $request){
        $title = 'Home';
        return view('main',['title' => $title]);
    }

    public function login(Request $request){
        return view('login');
    }

    public function templateFunction(Request $request,$name){
        $title = ucwords(strtolower($name));
        return view($name,['title' => $title]);
    }
}
