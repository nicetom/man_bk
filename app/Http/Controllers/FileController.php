<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Storage;
use App\Models\User;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function uploadProfileImage($image){
        $path = Storage::disk('public')->putFile('profile_images',$image);
        $file_name = explode('/',$path);
        return $file_name[1];
    }

    public function downloadProfileImage($user_id){
        $user = User::find($user_id);
        $image = $user->image;
        return Storage::download("public/profile_images/".$image->image_location);
    }
}
