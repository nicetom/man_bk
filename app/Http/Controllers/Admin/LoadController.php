<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Hash;
use Validator;
use Carbon\Carbon;
use Session;
use App\Models\Accounts;
use App\Models\Pins;
use App\Models\Admins;
use App\Models\Banks;
use App\Models\Transactions;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FileController;
use App\Models\User;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class LoadController extends Controller
{
    private $user;
    private $file;

    public function __construct(UserRepository $user, FileController $file)
    {
        $this->user = $user;
        $this->file = $file;
    }

    public function authenticate(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'email' => ['required', 'email', 'exists:admins'],
            'password' => ['required']
        ];

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors());
        } else {
            if (Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']], 1)) {
                return redirect()->route('admin_home');
            } else {
                Session::put('red', 1);
                return redirect()->route('admin_login')->withErrors(['password' => 'Wrong Password'])->withInput();
            }
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        Session::put('green', 1);
        return redirect()->route('admin_login')->withErrors(['message' => 'Logged Out Successfully'])->withInput();
    }

    public function createUser(Request $request)
    {
        $data = $request->except('_token');

        $rules = [
            'firstname' => ['required', 'alpha', 'min:2', 'max:20',],
            'lastname' => ['required', 'alpha', 'min:2', 'max:20',],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8', 'max:20', 'string'],
            'currency_id' => ['required', 'numeric', 'exists:currencies,id'],
            'status_id' => ['required', 'numeric', 'exists:statuses,id'],
            'profile_image' => ['required', 'image', 'mimes:png,jpeg,jpg']
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $location = $this->file->uploadProfileImage($data['profile_image']);
            if ($location) {
                unset($data['profile_image']);
                $data['admin_id'] = Auth::guard('admin')->user()->id;
                $data['firstname'] = ucfirst(strtolower($data['firstname']));
                $data['lastname'] = ucfirst(strtolower($data['lastname']));
                $data['password'] = Hash::make($data['password']);
                $user_success = $this->user->create($data);
                if ($user_success) {
                    $user_success->image()->create([
                        'image_location' => $location
                    ]);
                    Session::put('green', 1);
                    return redirect()->back()->withErrors(['message' => 'User Successfully created']);
                }
            }
        }
    }



    public function allocateAccount(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'user_id' => ['required', 'exists:users,id'],
            'account_type_id' => ['required', 'exists:account_types,id'],
            'bank_id' => ['required', 'exists:banks,id'],
            'account_number' => ['required', 'integer', 'digits_between:10,12'],
            'balance' => ['required', 'numeric', 'digits_between:2,18']
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $account_create = Accounts::create($data);
            if ($account_create) {
                Session::put('green', 1);
                return redirect()->back()->withErrors(['message' => 'Account Allocated Successfully']);
            } else {
                Session::put('red', 1);
                return redirect()->back()->withErrors(['message' => 'An Error Occured']);
            }
        }
    }

    public function getAccounts(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $returnArray = [];
        foreach ($user->accounts as $account) {
            array_push($returnArray, [
                'id' => $account->id,
                'bank' => $account->bank->name,
                'account_type' => $account->account_type->name,
                'account_number' => $account->account_number,
                'balance' => $account->balance
            ]);
        }
        return response()->json($returnArray);
    }

    public function createTransactionRecord(array $data)
    {
        return Transactions::create($data);
    }

    public function newTransaction(Request $request)
    {
        $data = $request->except(['_token', 'user_id']);
        $rules = [
            'account_id' => ['required', 'exists:accounts,id'],
            'bank_id' => ['required', 'exists:banks,id'],
            'status_id' => ['required', 'exists:statuses,id'],
            'transaction_type_id' => ['required', 'exists:transaction_types,id'],
            'account_name' => ['required', 'string'],
            'account_number' => ['required', 'numeric', 'digits_between:10,12'],
            'date_of_transaction' => ['required', 'date'],
            'description' => ['required', 'string', 'min:10'],
            'amount' => ['required', 'numeric']
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $account = Accounts::find($data['account_id']);
            $data['account_name'] = \ucwords(strtolower($data['account_name']));
            $permitted_chars = '0123456789';
            $random_pin = substr(str_shuffle($permitted_chars), 0, 10);
            $random_pin2 = substr(str_shuffle($permitted_chars), 0, 10);
            $data['routing_number'] = $random_pin;
            $data['swift_code'] = $random_pin2;

            $new_date = Carbon::parse($data['date_of_transaction']);

            $data['date_of_transaction'] = $new_date->format('Y-m-d');
            //dd($data['date_of_transaction']);


            if (($data['status_id'] == '2') || ($data['status_id'] == '3')) {
                $data['balance'] = $account->balance;
                $success = $this->createTransactionRecord($data);
                if ($success) {
                    Session::put('green', 1);
                    return redirect()->back()->withErrors(['message' => 'Transaction Recorded Successfully']);
                }
            } else if ($data['status_id'] == '1') {
                if ($data['transaction_type_id'] == '1') {
                    $new_balance = $account->balance + $data['amount'];
                    $account->balance = $new_balance;
                    $account->save();
                    $data['balance'] = $new_balance;
                } else if ($data['transaction_type_id'] == '2') {
                    if ($data['amount'] > $account->balance) {
                        Session::put('red', 1);
                        return redirect()->back()->withErrors(['error' => 'Amount exceeds the available balance for this account'])->withInput();
                    } else {
                        $new_balance = $account->balance - $data['amount'];
                        $account->balance = $new_balance;
                        $account->save();
                        $data['balance'] = $new_balance;
                    }
                }

                $this->createTransactionRecord($data);
                Session::put('green', 1);
                return redirect()->back()->withErrors(['message' => 'Transaction Recorded Successfully']);
            }
        }
    }

    public function deleteUser(Request $request)
    {
        $data = $request->except('_token');
        $delete_success = $this->user->delete($data['user_id']);
        Session::put('green', 1);
        return redirect()->back()->withErrors(['message' => 'User deleted successfully']);
    }

    public function updateUser(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'firstname' => ['required', 'alpha'],
            'lastname' => ['required', 'alpha'],
            'email' => ['required', 'email'],
            'currency_id' => ['required', 'numeric'],
            'status_id' => ['required', 'numeric'],
            'password' => ['nullable']
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            if (empty($data['password'])) {
                unset($data['password']);
                $user_id = $data['user_id'];
                unset($data['user_id']);
            } else {
                $data['password'] = Hash::make($data['password']);
                $user_id = $data['user_id'];
                unset($data['user_id']);
            }
            $update_success = $this->user->update($data, $user_id);

            Session::put('green', 1);
            return redirect()->back()->withErrors(['message' => 'User profile updated successfully']);
        }
    }

    public function createPin(Request $request)
    {
        $permitted_chars = '0123456789';
        $random_pin = substr(str_shuffle($permitted_chars), 0, 6);
        Pins::create([
            'pin' => $random_pin
        ]);

        Session::put('green', 1);
        return redirect()->back()->withErrors(['message' => 'Pin created successfully']);
    }

    public function createBank(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'name' => ['required', 'unique:banks', 'string']
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $data['name'] = \ucwords($data['name']);
            $success = Banks::create($data);
            if ($success) {
                Session::put('green', 1);
                return redirect()->back()->withErrors(['message' => 'Bank Created Successfully']);
            }
        }
    }

    public function updateBank(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'name' => ['required', 'string'],
            'bank_id' => ['required', 'exists:banks,id']
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $data['name'] = \ucwords($data['name']);
            $bank_id = $data['bank_id'];
            unset($data['bank_id']);
            $success = Banks::where('id', $bank_id)->update($data);
            if ($success) {
                Session::put('green', 1);
                return redirect()->back()->withErrors(['message' => 'Bank Updated Successfully']);
            }
        }
    }

    public function deleteBank(Request $request)
    {
        $data = $request->except('_token');
        $success = Banks::where('id', $data['bank_id'])->delete();

        Session::put('green', 1);
        return redirect()->back()->withErrors(['message' => 'Bank deleted Successfully']);
    }

    public function createAdmin(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'firstname' => ['required', 'alpha'],
            'lastname' => ['required', 'alpha'],
            'email' => ['required', 'email', 'unique:admins,email'],
            'password' => ['required', 'alpha'],
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $data['firstname'] = ucfirst(strtolower($data['firstname']));
            $data['lastname'] = ucfirst(strtolower($data['lastname']));
            $data['password'] = Hash::make($data['password']);
            $success = Admins::create($data);
            if ($success) {
                Session::put('green', 1);
                return redirect()->back()->withErrors(['message' => 'Admin Account Created Successfully']);
            }
        }
    }

    public function deleteTransaction(Request $request)
    {
        $id = $request['transaction_id'];
        $success = Transactions::where('id', $id)->delete();
        if ($success) {
            Session::put('green', 1);
            return redirect()->back()->withErrors(['message' => 'Transaction deleted Successfully']);
        }
    }

    public function editTransaction(Request $request)
    {
        $data = $request->except('_token');

        $rules = [
            'transaction_id' => ['required', 'exists:transactions,id'],
            'description' => ['required','string'],
            'status_id' => ['required', 'exists:statuses,id'],
            'date_of_transaction' => ['required', 'before_or_equal:'.date('Y-m-d',time())],
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {

            $id = $request['transaction_id'];
            unset($data['transaction_id']);

            $success = false;
            if (($data['status_id'] == '3') || ($data['status_id'] == '2')) {
                Transactions::where('id', $id)->update($data);
            } else if ($data['status_id'] == '1') {
                $transaction = Transactions::find($id);
                if ($transaction->transaction_type->id == '1') {
                    $account_balance = $transaction->account->balance;
                    $new_balance = $account_balance + $transaction->amount;
                    $transaction->account()->update([
                        'balance' => $new_balance
                    ]);
                    $data['balance'] = $new_balance;
                } else if ($transaction->transaction_type->id == '2') {
                    $account_balance = $transaction->account->balance;
                    $new_balance = $account_balance - $transaction->amount;
                    $transaction->account()->update([
                        'balance' => $new_balance
                    ]);
                    $data['balance'] = $new_balance;
                }

                Transactions::where('id', $id)->update($data);
            }


            Session::put('green', 1);
            return redirect()->back()->withErrors(['message' => 'Transaction Updated Successfully']);
        }
    }
}
