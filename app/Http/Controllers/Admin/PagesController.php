<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\Currency;
use App\Models\AccountType;
use App\Models\User;
use App\Models\Pins;
use App\Models\Banks;
use App\Models\TransactionTypes;
use App\Models\Transactions;
use App\Models\Status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function login(Request $request)
    {
        return view('admin.login');
    }

    public function dashboard(Request $request)
    {
        $title = 'Dashboard';
        return view('admin.restricted.home', ['title' => $title]);
    }

    public function createUser(Request $request)
    {
        $title = 'Create User';
        $statuses = Status::all();
        $currencies = Currency::all();
        return view('admin.restricted.new_user', ['title' => $title, 'statuses' => $statuses, 'currencies' => $currencies]);
    }

    public function allocateAccount(Request $request)
    {
        $title = 'Allocate Accounts';
        $types = AccountType::all();
        $banks = Banks::all();
        $users =  User::where('admin_id', Auth::guard('admin')->user()->id)->get();
        return view('admin.restricted.allocate_accounts', ['title' => $title, 'account_types' => $types, 'users' => $users, 'banks' => $banks]);
    }

    public function newTransaction(Request $request)
    {
        $title = 'Add Transactions';
        $banks = Banks::all();
        $types = TransactionTypes::all();
        $statuses = Status::all();
        $users =  User::where('admin_id', Auth::guard('admin')->user()->id)->has('accounts')->get();
        return view('admin.restricted.new_transaction', ['title' => $title, 'users' => $users,'banks' => $banks,'transaction_types' => $types,'statuses' => $statuses]);
    }

    public function users(Request $request){
        $title = 'Users';
        $statuses = Status::all();
        $currencies = Currency::all();
        $users = Auth::guard('admin')->user()->created_users()->with(['currency','status','accounts'])->get();
        return view('admin.restricted.users', ['title' => $title, 'users' => $users,'statuses' => $statuses, 'currencies' => $currencies]);
    }

    public function pinPage(Request $request){
        $title = 'Active Pins';
        $pins = Pins::where('status','0')->get();
        return view('admin.restricted.pins', ['title' => $title,'pins' => $pins]);
    }

    public function createBank(Request $request){
        $title = 'Banks';
        $banks = Banks::all();
        return view('admin.restricted.banks', ['title' => $title,'banks' => $banks]);
    }

    public function createAdmin(Request $request){
        $title = 'New Admin';
        return view('admin.restricted.new_admin', ['title' => $title]);
    }

    public function transactionPage(Request $request){
        $title = 'Transactions';
        $transactions = Transactions::whereHas('account',function($query){
            $query->whereHas('user',function($query1){
                $query1->where('admin_id',Auth::guard('admin')->user()->id);
            });
        })->get();
        $statuses = Status::all();
        return view('admin.restricted.transactions', ['title' => $title,'transactions' => $transactions,'statuses' => $statuses]);
    }
}
