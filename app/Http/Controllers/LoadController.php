<?php

namespace App\Http\Controllers;
use Validator;
use Auth;
use App\Http\Controllers\FileController;
use Session;
use Illuminate\Http\Request;

class LoadController extends Controller
{
    private $file;

    public function __construct(FileController $file){
        $this->file = $file;
    }

    public function login(Request $request){
        $data = $request->except('_token');
        $rules = [
            'email' => ['required','email','exists:users'],
            'password' => ['required']
        ];
        $validator = Validator::make($data,$rules);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator->errors());
            }else{
                if(Auth::attempt(['email' => $data['email'],'password' => $data['password']],1)){
                    return redirect()->route('verification');
                }else{
                    return redirect()->back()->withErrors(['password' => 'Password is not correct'])->withInput();
                }
            }
    }

    public function getImage(Request $request, $user_id)
    {
        return $this->file->downloadProfileImage($user_id);
    }


}
