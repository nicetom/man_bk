<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Banks;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function homePage(Request $request){
        $title = 'Home';
        return view('user.home', ['title' => $title]);
    }

    public function transferPage(Request $request){
        $title = 'Transfer';
        $banks = Banks::all();
        return view('user.transfer', ['title' => $title,'banks' => $banks]);
    }

    public function verification(Request $request){
        return view('verification');
    }
}
