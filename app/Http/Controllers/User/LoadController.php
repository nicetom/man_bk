<?php

namespace App\Http\Controllers\User;

use Validator;
use Session;
use Auth;
use App\Models\Accounts;
use App\Models\Pins;
use App\Models\Transactions;
use App\Http\Controllers\Admin\LoadController as AdminFunction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoadController extends Controller
{
    private $admin;

    public function __construct(AdminFunction $admin)
    {
        $this->admin = $admin;
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('home_page');
    }


    public function transfer(Request $request)
    {

        try {

            $data = $request->except('_token');
            $rules = [
                'account_id' => ['required', 'exists:accounts,id'],
                'bank_id' => ['required', 'exists:banks,id'],
                'account_name' => ['required', 'string'],
                'account_number' => ['required', 'numeric', 'digits_between:10,12'],
                'routing_number' => ['nullable', 'numeric', 'digits:9'],
                'swift_code' => ['nullable', 'numeric', 'digits_between:9,12'],
                'amount' => ['required', 'numeric'],
                'description' => ['required', 'string'],
            ];

            $messages = [
                'account_number.digits_between' => 'Invalid Account Number',
                'routing_number.digits' => 'Invalid Routing Number',
                'swift_code.digits_between' => 'Invalid Routing Number',
            ];

            $validation = Validator::make($data, $rules, $messages);
            if ($validation->fails()) {
                Session::put('red', 1);
                return redirect()->back()->withErrors($validation->errors())->withInput();
            } else {
                $account = Accounts::find($data['account_id']);
                $balance = $account->balance;
                if (empty($data['routing_number'])) {
                    $permitted_chars = '0123456789';
                    $random_pin = substr(str_shuffle($permitted_chars), 0, 10);
                    $data['routing_number'] = $random_pin;
                }

                if (empty($data['swift_code'])) {
                    $permitted_chars = '0123456789';
                    $random_pin = substr(str_shuffle($permitted_chars), 0, 10);
                    $data['swift_code'] = $random_pin;
                }

                if ($data['amount'] < $balance) {
                    unset($data['account_id']);
                    $data['account_name'] = ucwords(\strtolower($data['account_name']));
                    $data['status_id'] = Auth::user()->status_id;
                    $data['transaction_type_id'] = '2';
                    $data['date_of_transaction'] = date('Y-m-d', time());
                    if (Auth::user()->status_id == '1') {
                        $data['balance'] = $balance - $data['amount'];
                        $account->balance = $data['balance'];
                        $account->save();

                        $account->transactions()->create($data);
                        Session::put('green', 1);
                        return redirect()->route('user_home')->withErrors(['message' => 'Transfer was successfully']);
                    } else {
                        $data['balance'] = $balance;
                        $account->transactions()->create($data);
                        if (Auth::user()->status_id == '2') {
                            if($data['tries'] == 2){
                                Session::put('red', 1);
                                return redirect()->route('user_home')->withErrors(['message' => 'Please Contact your Account Officer']);
                            }else{
                                Session::put('red', 1);
                                return redirect()->back()->withErrors(['message' => 'Error, Please Try Again.','tries' => '2']);
                            }

                        } else if (Auth::user()->status_id == '3') {
                            Session::put('blue', 1);
                            return redirect()->route('user_home')->withErrors(['message' => 'Transfer process has been initiated. You"ll notified on the transaction progress']);
                        }
                    }
                } else {
                    Session::put('red', 1);
                    return redirect()->back()->withErrors(['message' => 'Amount exceeds balance'])->withInput();
                }
            }
        }
         catch (\Exception $e) {
            Session::put('red', 1);
            return redirect()->back()->withErrors(['message' => 'Oops, An Error Occured. Please Try again later'])->withInput();
        }
    }

    public function getTransactions(Request $request, $account_id)
    {
        $transactions = Transactions::where('account_id', $account_id)->orderBy('date_of_transaction', 'DESC')->paginate(10);
        return response()->json($transactions);
    }

    public function verification(Request $request)
    {
        $data = $request->except('_token');
        $rules = [
            'pin' => ['required', 'numeric', 'exists:pins,pin']
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            Session::put('red', 1);
            return redirect()->back()->withErrors($validation->errors())->withInput();
        } else {
            $pin = Pins::where('pin',$data['pin'])->first();
                if($pin->status == '1'){
                    return redirect()->back()->withErrors(['pin' => 'Invalid Pin']);
                }else{
                    $pin->status = '1';
                    $pin->save();
                    Session::put('green', 1);
                    return redirect()->route('user_home')->withErrors(['message' => 'Welcome ' . Auth::user()->firstname . ' ' . Auth::user()->lastname]);
                }

        }
    }
}
