<?php

namespace App\Repositories\User;

interface UserInterface {

    public function create(array $data);

    public function delete($user_id);

    public function update(array $data,$user_id);
}
