<?php

namespace App\Repositories\User;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;

class UserRepository implements UserInterface{

    public function create(array $data){
        return User::create($data);
    }

    public function delete($user_id){
        $user = User::find($user_id);
        return $user->delete();
    }

    public function update(array $data,$user_id){
        return User::where('id',$user_id)->update($data);
    }

}
