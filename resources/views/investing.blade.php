@extends('includes.master')

@section('content')

<main id="content" class=" cards background-6">

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
      <div class="hero-body text-hero">

        <div class="container text-centered">
          <h1 class="title is-1 is-spaced reduced-margin text-maroon">
            Personal Investing





          </h1>





        </div>

      </div>
    </section>

    <section class="promo-banner background-color-red aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

    <div class="promo-image center-top hidden-xs-down" style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-12.jpg?ext=.jpg')"></div>



    <div class="promo-image background-image center-top hidden-xs-up" style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-12-small.jpg?ext=.jpg')"></div>


    <div class="promo-content contactus-banner">
      <h3 class="title is-3 white">Plan for the future and feel at ease. Your goals are our goals, and you’re always in expert hands.</h3>
      <p class="promo-p hidden-lg-down white">

      </p>

      <a class="cta-link fill-primary" href="#">
        <div class="cta-wrapper">
          <span class="cta-text ">Start the conversation</span>
          <div class="cta-img dark-arrow">
          </div>
        </div>
      </a>

    </div>




  </section>

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350" style="display: none;">
      <div class="hero-body text-hero">

        <div class="container text-centered">

        </div>

      </div>
    </section>


    <section class="creditCard-section aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

      <div class="grid-container">
        <div class="columns is-multiline card-columns">


  <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/Investment-Certificates.svg?ext=.svg" alt="Guaranteed Investment Certificates (GIC)">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Guaranteed Investment Certificates (GIC)
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members who want a simple, low-risk investing option. No matter the term, we’ll find the GIC that’s right for you.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All GICs</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/tax-free-dark.svg?ext=.svg" alt="Tax-Free Savings Account (TFSA)">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Tax-Free Savings Account (TFSA)
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p><br>
  Best for members who want tax-free growth on their savings. A TFSA can hold a wide variety of investment products.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All TFSAs</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/growth.svg?ext=.svg" alt="Registered Retirement Savings Plan (RRSP)">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Registered Retirement Savings Plan (RRSP)
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members planning their long-term retirement savings. No matter your goals, we can help set you on a path to a successful retirement.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All RRSPs</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/Education-Savings.svg?ext=.svg" alt="Registered Education Savings Plan (RESP)">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Registered Education Savings Plan (RESP)
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members who want to contribute to a child’s education. Watch your investment grow with access to government grants and great interest rates.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All RESPs</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/Income-Fund.svg?ext=.svg" alt="Registered Retirement Income Fund (RRIF)">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Registered Retirement Income Fund (RRIF)
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members with RRSP savings and who are close to retirement. Roll your RRSP over to a RRIF, save money, and put your savings to work for you.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All RRIFs</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/Disability-Savings.svg?ext=.svg" alt="Registered Disability Savings Plan (RDSP)">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Registered Disability Savings Plan (RDSP)
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members and their families who are saving for future living and medical expenses. Access government grants and great interest rates with no impact on social benefits.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See RDSPs</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/Online.svg?ext=.svg" alt="Online Investing">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Online Investing
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>&nbsp;</p>

  <p>Best for members who want easy, low-cost investing. Choose Qtrade Investor for self-directed investing or VirtualWealth<sup>®</sup>&nbsp; robo‑advisor for a more guided experience*.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">Learn More</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div>

        </div>
      </div>

    </section>
    <section class="section-spacing mortgage-section Account-Detail-content-title" style="display: none;">
      <div class="grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

      <div class="mortgage-wrapper" style="Width:100%;">
          <div class="description">





        </div>
        </div>

      </div>
    </section>



    <section class="terms-and-conditions aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="grid-container">
      <div class="viewport-spacing">
      <p>*Online brokerage services are offered through Qtrade Investor, a division of Credential Qtrade Securities Inc.</p>

  <p>®VirtualWealth is a trade name of Credential Qtrade Securities Inc.</p>





      </div>
    </div>
  </section>

  <div class="background-color-red aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="grid-container">
      <div class="columns dingFree-banner">

        <div class="column is-7">
          <p>Sign up to receive monthly Market Insights. Get stock reports and insights right to your inbox.</p>

        </div>

        <div class="column is-5">
        <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz">
      <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_pM_pMP">

      </div><div id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_pnlForm" class="FormPanel" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_btnOK')">
          <div class="scu-searchbar" id="market-insights-searchbar"><div id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_ncpemail" class="EditingFormControlNestedControl editing-form-control-nested-control">
              <input name="p$lt$ctl12$pageplaceholder$p$lt$ctl09$On_lineForm$viewBiz$Email$txtText" type="text" maxlength="200" id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_Email_txtText" class="form-control form-control searchbar-input informed-input form-control searchbar-input informed-input WatermarkText form-control searchbar-input informed-input WatermarkText form-control form-control searchbar-input informed-input form-control searchbar-input informed-input WatermarkText form-control searchbar-input informed-input WatermarkText WatermarkText" aria-label="Stay informed with our newsletter">
  <input type="hidden" name="p$lt$ctl12$pageplaceholder$p$lt$ctl09$On_lineForm$viewBiz$Email$exWatermark_ClientState" id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_Email_exWatermark_ClientState">
          </div> <input type="submit" name="p$lt$ctl12$pageplaceholder$p$lt$ctl09$On_lineForm$viewBiz$btnOK" value="Sign Up" id="p_lt_ctl12_pageplaceholder_p_lt_ctl09_On_lineForm_viewBiz_btnOK" class="FormButton btn btn-primary"></div>

      </div>
  </div>

        </div>
      </div>
    </div>
  </div>


  </main>
@endsection
