@extends('includes.master')

@section('content')

<main id="content" class=" cards background-6">

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
      <div class="hero-body text-hero">

        <div class="container text-centered">
          <h1 class="title is-1 is-spaced reduced-margin text-maroon">
            Business &amp; Agricultural Accounts
          </h1>
        </div>

      </div>
    </section>




    <section class="promo-banner background-color-red aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

    <div class="promo-image center-top hidden-xs-down" style="background-image: url({{asset('assets/main/SCU/media/Images/Call-out-banner-10.jpg?ext=.jpg')}})"></div>



    <div class="promo-image background-image center-top hidden-xs-up" style="background-image: url({{asset('assets/main/SCU/media/Images/Call-out-banner-10-small.jpg?ext=.jpg')}})"></div>


    <div class="promo-content contactus-banner">
      <h3 class="title is-3 white">Take your business to the next level, with banking solutions and expert advice that will help you get there.</h3>
      <p class="promo-p hidden-lg-down white">

      </p>

    </div>




  </section>

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350" style="display: none;">
      <div class="hero-body text-hero">

        <div class="container text-centered">

        </div>

      </div>
    </section>


    <section class="creditCard-section aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

      <div class="grid-container">
        <div class="columns is-multiline card-columns">


  <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/piggy-bank-dark.svg?ext=.svg" alt="Savings Accounts">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Savings Accounts
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p class="bullet-point card-list-item">Save for future business expenses or expansions. Earn daily or monthly interest at excellent rates. Get low monthly fees and expert financial guidance.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All Savings Accounts</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/check-dark.svg?ext=.svg" alt="Chequing Accounts">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Chequing Accounts
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p class="bullet-point card-list-item">Day-to-day banking for businesses and farms of all sizes. Dedicated tellers, low monthly fees, and 24-hour drive thru ATMs are standard with every account. We make it easy, so you can run efficiently and be more profitable.</p>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All Chequing Accounts</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div>
  <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/money-flower-dark.svg?ext=.svg" alt="Youth Accounts">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Youth Accounts
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <ul class="card-list">
      <li class="bullet-point card-list-item">
      <p>Help your child develop their money management skills</p>
      </li>
      <li class="bullet-point card-list-item">
      <p>Choose from a savings account for children under 14 or spending account for youth under age 18</p>
      </li>
      <li class="bullet-point card-list-item">
      <p>Earn interest, with no monthly fees and no minimum balance requirements</p>
      </li>
  </ul>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All Youth Accounts</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div>
  <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/grad-cap-dark.svg?ext=.svg" alt="Student Accounts">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Student Accounts
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <ul class="card-list">
      <li>Affordable banking for post-secondary students at any age</li>
      <li>
      <p>Access to a full suite of online and digital banking solutions</p>
      </li>
      <li>
      <p>Proof of enrollment is required</p>
      </li>
  </ul>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All Student Accounts</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div>

  <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/senior-account.svg?ext=.svg" alt="Senior Accounts">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Senior Accounts
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <ul class="card-list card-underline">
      <li class="bullet-point card-list-item">
      <p>It’s always a good time to find smarter banking solutions</p>
      </li>
      <li class="bullet-point card-list-item">
      <p>Our members aged 60+ receive premium perks and no fees</p>
      </li>
      <li class="bullet-point card-list-item">
      <p>Enjoy the savings you’ve worked hard to build<br>
      &nbsp;</p>
      </li>
  </ul>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All Senior Accounts</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div>

        </div>
      </div>

    </section>
    <section class="section-spacing mortgage-section Account-Detail-content-title">
      <div class="grid-container">

      <div class="mortgage-wrapper" style="Width:100%;">
          <div class="description">
          <h2>Features Included with all our Business and Agricultural Accounts</h2>
  &nbsp;

  <h3>100% Deposit Protection</h3>

  <p>All deposits are 100% covered by the Deposit Guarantee Corporation of Manitoba.</p>

  <h3><br>
  No Minimum Balance</h3>

  <p>{{config('app.name')}} business accounts never have a minimum monthly balance requirement.</p>

  <h3><br>
  Enhanced Service</h3>

  <p>Dedicated teller stations for business and agricultural accounts.</p>

  <h3><br>
  24/7 Business Banking</h3>

  <p>Night deposits, 24-hour ATM drive through, and online banking are all standard with every business account.</p>

  <h3><br>
  Financial Advice</h3>

  <p>We’re experts in our local market who are dedicated to your success. Together, we’ll make sure your business grows.</p>





        </div>
        </div>

      </div>
    </section>


    <section class="background-color-red aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
        <div class="container">
          <div class="columns faq-banner">

            <div class="column is-6 faq-banner-column">
              <h3 class="title is-3 text-white">Questions? <br class="hidden-md-up"> We're here for you.</h3>
            </div>

            <div class="column is-offset-1 is-5 mg-auto">
              <ul class="banner-link-wrapper">
                <li>
                  <a class="banner-link" href="#">

                    <img src="{{url('/')}}/assets/main/SCU/media/Images/phone-white.png?ext=.png" alt="1.800.728.6440">

                    <span>1.800.728.6440</span></a>
                </li>
                <li>
                  <a class="banner-link" href="#">
                    <img src="{{url('/')}}/assets/main/SCU/media/Images/mail-white.png?ext=.png" alt="Email Us">

                    <span>Email Us</span></a>
                </li>
                <li>
                  <a class="banner-link" href="#">
                    <img src="{{url('/')}}/assets/main/SCU/media/Images/bank-white.png?ext=.png" alt="Find a Branch">

                    <span>Find a Branch</span></a>
                </li>
              </ul>
            </div>

          </div>
        </div>

      </section>
    <section class="terms-and-conditions aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350" style="display: none;">
    <div class="grid-container">
      <div class="viewport-spacing">
      </div>
    </div>
  </section>

  </main>
@endsection
