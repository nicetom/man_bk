@extends('template.master')

@section('content')

<div class="row">
    <div class="col-md-10 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Accounts</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="tr-class">
                                <th> Account Type </th>
                                <th> Account Number </th>
                                <th> Balance </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (Auth::user()->accounts as $account)
                            <tr>
                                <td>{{$account->account_type->name}}</td>
                                <td>{{$account->account_number}}</td>
                                <td>{{Auth::user()->currency->symbol}}{{number_format($account->balance,2,'.',',')}}
                                </td>
                                <td><button id="{{$account->id}}" class="btn btn-gradient-primary btn-sm recent">Recent
                                        Transactions</button></td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Transactions</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr class="tr-class">
                                <th>#</th>
                                <th>Date</th>
                                <th> Description </th>
                                <th> Status </th>
                                <th> Type </th>
                                <th> Amount ({{Auth::user()->currency->symbol}}) </th>
                                <th> Balance ({{Auth::user()->currency->symbol}})</th>
                            </tr>
                        </thead>
                        <tbody id="transaction_content">
                            <tr>
                                <td colspan="7" style="text-align:center;">
                                    No Account Selected Yet
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <table id="pagination" class="hide">
                            <tr>
                                <td>
                                     <ul class="pagination">
                                       <li class="page-item"><a class="page-link" id="previous" href="#">Previous</a></li>
                                       <li class="page-item"><a class="page-link" id="content" disabled></a></li>
                                       <li class="page-item"><a class="page-link" id="next" href="#">Next</a></li>
                                     </ul>
                                </td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('other_scripts')
<script src="{{asset('assets/admin/js/transactions.js')}}"></script>
@endsection
