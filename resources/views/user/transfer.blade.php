@extends('template.master')

@section('content')

<div class="col-md-8 offset-md-2 col-sm-12 col-xs-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Transfer</h4>
            <form action="{{route('transfer_action')}}" method="POST" autocomplete="off">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Account</label>
                        <select name="account_id" class="form-control">
                            <option value="">Select an Account</option>
                            @foreach (Auth::user()->accounts as $account)
                            <option @if(old('account_id') == $account->id) selected @endif value="{{$account->id}}">{{$account->account_number}} -
                                {{$account->account_type->name}} - {{Auth::user()->currency->symbol}}
                                {{$account->balance}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Bank</label>
                        <select name="bank_id" class="form-control">
                            <option value="">Select a Bank</option>
                            @foreach ($banks as $bank)
                            <option @if(old('bank_id') == $bank->id) selected @endif value="{{$bank->id}}">{{$bank->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Account Name</label>
                        <input type="text" name="account_name" class="form-control" placeholder="Account Name" value="{{old('account_name')}}">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Account Number</label>
                        <input type="number" name="account_number" class="form-control" placeholder="Account Number" value="{{old('account_number')}}">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Amount</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span
                                    class="input-group-text bg-gradient-dark text-white">{{Auth::user()->currency->symbol}}</span>
                            </div>
                            <input type="number" name="amount" class="form-control" value="{{old('amount')}}">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Routing Number</label>
                        <input type="number" name="routing_number" class="form-control" placeholder="Routing Number" value="{{old('routing_number')}}">
                    </div>
                    @if($errors->has('tries'))
                    <input type="hidden" name="tries" value="2">
                    @else
                    <input type="hidden" name="tries" value="1">
                    @endif
                    <div class="form-group col-md-6">
                        <label>Swift Code (Optional)</label>
                        <input type="number" name="swift_code" class="form-control" placeholder="Swift Code" value="{{old('swift_code')}}">
                    </div>

                    <div class="form-group col-md-12">
                        <label>Description</label>
                        <textarea type="number" name="description" class="form-control" placeholder="Description"
                            rows="5" >{{old('description')}}</textarea>
                    </div>
                </div>

                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>

            </form>
        </div>
    </div>
</div>

@endsection
