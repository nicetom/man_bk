@extends('includes.master')

@section('content')
<main id="content" class="cards background-g">

    <div class="hero grid-container  aos-animate">
        <div class="hero-body text-hero no-body">

            <div class="container text-centered">
                <h1 class="title is-1 is-spaced reduced-margin text-maroon">
                    Contact {{config('app.name')}}




                </h1>
            </div>

        </div>
    </div>
    <section class="single-contact-banner  aos-animate">
        <div class="banner-column background-color-red">
            <div class="banner-content">
                <h3 class="title is-3">Member Contact Centre</h3>

                <p class="contact-content">Call our Member Contact Centre to get in touch with knowledgeable staff.
                    We’re ready to take your call on a wide variety of products and services. Even save yourself a
                    personal visit to one of our branches by using your member identification code (MIC) to make
                    personal, secure transactions when you call the Member Contact Centre.<br>
                    <br>
                    Mon - Fri&nbsp; 8:00 a.m. - 8:00 p.m. (CT)<br>
                    Sat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;8:00 a.m. - 4:00 p.m. (CT)</p>





                <div class="contact-info">
                    <p>
                        Hotline &nbsp; &nbsp; &nbsp;<strong><a href="tel:929-242-9793">929-242-9793</a></strong><br>
                        Toll-free &nbsp; &nbsp;&nbsp;<strong><a href="tel:888-723-0735">888-723-0735</a></strong></p>

                        <p>
                           Address &nbsp; &nbsp; &nbsp;<strong>Madison Ave, 20th Floor, New York, NY 10022</strong>
                        </p>
                        <br>
                        <p>Contact our Customer Service on <a href="mailto:info@citizenscorp.org" style="color:red">info@citizenscorp.org</a> or <a style="color:red" href="mailto:customerservices@citizenscorp.org">customerservices@citizenscorp.org</a></p>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="contact-form  aos-animate">

        <div id="contact-form" role="form">

            <div class="form-header">
                <h2 class="title is-4">We'd love to hear from you!



                </h2>
                <div class="form-description">
                    <p>Please fill out this form to submit your question or comment. &nbsp;<br>
                        <br>
                        Sincerely, &nbsp;<br>
                        Glenn Friesen, CEO<br>
                        &nbsp;</p>





                </div>
            </div>


            <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_sys_pnlUpdate">
                <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz">
                    <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_pnlForm" class="FormPanel"
                        onkeypress="javascript:return WebForm_FireDefaultButton(event, 'p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_btnOK')">
                        <ul class="input-section section-form-input">
                            <li>
                                <div class="form-label">
                                    <p>Select a Topic <span class="important">*</span></p>

                                    <p class="error is-required" role="alert">Required Field</p>
                                </div>
                                <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_ncptopic"
                                    class="EditingFormControlNestedControl editing-form-control-nested-control">
                                    <select
                                        name="p$lt$ctl12$pageplaceholder$p$lt$ctl05$On_lineForm$plcUp$viewBiz$Topic$dropDownList"
                                        id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_Topic_dropDownList"
                                        class="DropDownField form-control">
                                        <option selected="selected" value="scu@scu.mb.ca:ProductsandServices">Products
                                            &amp; Services</option>
                                        <option value="scu@scu.mb.ca:TechnicalIssues">Technical Issues</option>
                                        <option value="scu@scu.mb.ca:AccountInformation">Account Information</option>
                                        <option value="scu@scu.mb.ca:SupportforOnlineServices">Support for Online
                                            Services</option>
                                        <option value="sponsorships@scu.mb.ca:SponsorshipInquiry">Sponsorship Inquiry
                                        </option>
                                        <option value="feedback@scu.mb.ca:Feedback">Feedback</option>
                                        <option value="ceo@scu.mb.ca:ContactCEO">Contact CEO</option>
                                        <option value="scudirectors@scu.mb.ca:ConfidentialBoardContact">Confidential
                                            Board Contact</option>
                                        <option value="accessibility@scu.mb.ca:Accessibility">Accessibility</option>

                                    </select>
                                    <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_Topic_autoComplete"
                                        class="autocomplete">


                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form-label">
                                    <p>Name <span class="important">*</span></p>

                                    <p class="error is-required" data-module-for="name" data-module-validate="required"
                                        role="alert">Required Field</p>
                                </div>
                                <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_ncpname"
                                    class="EditingFormControlNestedControl editing-form-control-nested-control">
                                    <input
                                        name="p$lt$ctl12$pageplaceholder$p$lt$ctl05$On_lineForm$plcUp$viewBiz$Name$html5input"
                                        type="text"
                                        id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_Name_html5input"
                                        class="form-control" maxlength="25">
                                </div>

                                <div class="Invalid" style="display:none">Please enter your name</div>
                            </li>
                            <li>
                                <div class="form-label">
                                    <p>Email <span class="important">*</span></p>

                                    <p class="error is-required" data-module-for="email" data-module-validate="required"
                                        role="alert">Required Field</p>
                                </div>
                                <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_ncpemail"
                                    class="EditingFormControlNestedControl editing-form-control-nested-control">
                                    <input
                                        name="p$lt$ctl12$pageplaceholder$p$lt$ctl05$On_lineForm$plcUp$viewBiz$Email$txtText"
                                        type="text" maxlength="200"
                                        id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_Email_txtText"
                                        class="form-control">

                                </div>

                                <p class="error is-required" data-module-for="name" data-module-validate="required"
                                    role="alert"></p>
                            </li>
                        </ul>


                        <div class="input-section">
                            <div class="comments">
                                <div class="form-label">
                                    <p>Comments or Questions</p>

                                    <p class="error is-required" role="alert">Required Field</p>
                                </div>
                                <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_ncpmessage"
                                    class="EditingFormControlNestedControl editing-form-control-nested-control">
                                    <textarea
                                        name="p$lt$ctl12$pageplaceholder$p$lt$ctl05$On_lineForm$plcUp$viewBiz$Message$txtText"
                                        rows="2" cols="20"
                                        id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_Message_txtText"
                                        class="form-control"></textarea>

                                </div>
                            </div>


                            <div class="reCAPTCHA">
                                <div class="g-recaptcha">
                                    <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_ncprecaptcha"
                                        class="EditingFormControlNestedControl editing-form-control-nested-control">
                                        <div
                                            id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_reCaptcha_captcha">
                                            <div
                                                id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_reCaptcha_captcha_pnlRecaptchaControl">
                                                <div id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_reCaptcha_captcha_pnlCaptchaWrap"
                                                    class="cms-recaptcha-wrap" data-rendersettings="{
    &quot;sitekey&quot;: &quot;6LdnYacUAAAAANBAgenHsslaIxclA4h2AqqOtQCp&quot;,
    &quot;theme&quot;: &quot;light&quot;,
    &quot;type&quot;: &quot;image&quot;,
    &quot;size&quot;: &quot;normal&quot;
  }">
                                                    <div style="width: 304px; height: 78px;">
                                                        <div><iframe
                                                                src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LdnYacUAAAAANBAgenHsslaIxclA4h2AqqOtQCp&amp;co=aHR0cHM6Ly93d3cuc2N1Lm1iLmNhOjQ0Mw..&amp;hl=en&amp;type=image&amp;v=wk6lx42JIeYmEAQSHndnyT8Q&amp;theme=light&amp;size=normal&amp;cb=38lgt0shrgag"
                                                                width="304" height="78" role="presentation"
                                                                name="a-z4citv7qf3ib" frameborder="0" scrolling="no"
                                                                sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
                                                        </div><textarea id="g-recaptcha-response"
                                                            name="g-recaptcha-response" class="g-recaptcha-response"
                                                            style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
                                                    </div><iframe style="display: none;"></iframe>
                                                </div><noscript>
                                                    <div>
                                                        <div style="width: 302px; height: 422px; position: relative;">
                                                            <div
                                                                style="width: 302px; height: 422px; position: absolute;">
                                                                <iframe
                                                                    src="https://www.google.com/recaptcha/api/fallback?k=6LdnYacUAAAAANBAgenHsslaIxclA4h2AqqOtQCp&hl=en"
                                                                    frameborder="0" scrolling="no"
                                                                    style="width: 302px; height:422px; border-style: none;">
                                                                </iframe>
                                                            </div>
                                                        </div>
                                                        <div style="width: 300px; height: 60px; border-style: none;
                     bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
                     background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                                                            <textarea id="g-recaptcha-response"
                                                                name="g-recaptcha-response" class="g-recaptcha-response"
                                                                style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                            margin: 10px 25px; padding: 0px; resize: none;">
        </textarea>
                                                        </div>
                                                    </div>
                                                </noscript>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="error"></div>


                            <div class="cta-link fill-primary">
                                <div class="cta-wrapper"><input type="submit"
                                        name="p$lt$ctl12$pageplaceholder$p$lt$ctl05$On_lineForm$plcUp$viewBiz$btnOK"
                                        value="Submit"
                                        id="p_lt_ctl12_pageplaceholder_p_lt_ctl05_On_lineForm_plcUp_viewBiz_btnOK"
                                        class="FormButton btn btn-primary">
                                    <div class="cta-img dark-arrow"><span style="display: none;">--</span></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <p style="text-align: right;"><br>
                <em><small><sub>There may be a 5-10 second<br>
                            delay after clicking Submit.</sub></small></em></p>

            <div style="text-align: right;"><a id="hours" name="hours"></a></div>







        </div>

    </section> --}}


    <section class="promo-banner background-color-red ">

        <div class="promo-image center hidden-xs-down"
            style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-8.jpg?ext=.jpg')">
        </div>



        <div class="promo-image background-image center-top hidden-xs-up"
            style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-8-small.jpg?ext=.jpg')">
        </div>


        <div class="promo-content contactus-banner">
            <h3 class="title is-3 white">Lost or Stolen Card?</h3>
            <p class="promo-p hidden-lg-down white">
                If you've lost your card or realize it's been stolen, click below to find out who to call.
            </p>

            <a class="cta-link fill-primary" href="#"
                target="_blank">
                <div class="cta-wrapper">
                    <span class="cta-text ">Learn more</span>
                    <div class="cta-img dark-arrow">
                    </div>
                </div>
            </a>

        </div>




    </section>

</main>
@endsection
