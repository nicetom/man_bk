@extends('includes.master')

@section('content')
<main id="content" class="cards background-6">

    <section class="hero grid-container ">
        <div class="hero-body text-hero">

            <div class="container text-centered">
                <h1 class="title is-1 is-spaced reduced-margin text-maroon">
                </h1>
                <h1>Personal Mortgages</h1>

                <p><br>
                    Choose from the following mortgage options. Some conditions may apply.<br>
                    &nbsp;</p>

            </div>

        </div>
    </section>

    <section class="promo-banner background-color-red ">

        <div class="promo-image center-top hidden-xs-down"
            style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-20.jpg?ext=.jpg')">
        </div>



        <div class="promo-image background-image center-top hidden-xs-up"
            style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-20.jpg?ext=.jpg')">
        </div>


        <div class="promo-content contactus-banner">
            <h3 class="title is-3 white">Applying online is easy!</h3>
            <p class="promo-p hidden-lg-down white">
                Take the first step today.
            </p>

            <a class="cta-link fill-primary"
                href="#"
                target="_blank">
                <div class="cta-wrapper">
                    <span class="cta-text ">Start your application</span>
                    <div class="cta-img dark-arrow">
                    </div>
                </div>
            </a>

        </div>




    </section>

    <h2 style="text-align: center;">Mortgage Options</h2>
    <br>
    &nbsp;





    <section class="creditCard-section ">

        <div class="grid-container">
            <div class="columns is-multiline card-columns">


                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/home-dark.svg?ext=.svg"
                                        alt="Home Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Home Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>Buying a home is a big decision, and we’re here for you every step of the way. Enjoy some
                                of the most competitive interest rates available and a variety of term, pre-payment, and
                                amortization options.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">See All Options </span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/home-reno.svg?ext=.svg"
                                        alt="Self-Build Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Self-Build Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>We have options specially designed for when you have the vision and the skills to build
                                your next home yourself. Our lending specialists have all the financing advice you need
                                when constructing your home.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">See All Options</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/Contractor2.svg?ext=.svg"
                                        alt="Contract-Build Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Contract-Build Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>Building a custom home is exciting, but it can also dig up complex financial challenges.
                                We have the financial expertise to make things simple for you, every step of the way.
                            </p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">See All Options</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/paint-brush.svg?ext=.svg"
                                        alt="Renovation Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Renovation Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>Why move when you can transform your current home into your dream home? Be ready to take
                                on this new challenge at a time and pace that’s right for you.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">See All Options</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/map-canada.svg?ext=.svg"
                                        alt="New-to-Canada Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        New-to-Canada Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>You’ve moved to Canada to seek a better life for you and your family. Explore options
                                designed to help you on your journey toward home ownership in Canada.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">Learn More</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/vacation.svg?ext=.svg"
                                        alt="Vacation Property Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Vacation Property Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>We’re here when you’re ready to realize your dream of owning a vacation property. Whether
                                it’s a cottage on the lake or a condo in the city, we’ve got options for all kinds of
                                real estate&nbsp;dreams.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">See All Options</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile ">
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/rental-property.svg?ext=.svg"
                                        alt="Rental Property Mortgages">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Rental Property Mortgages
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p>For personal investors looking to build a portfolio of rental properties. Discover a
                                range of solutions that make becoming a real estate investor as feasible as being a
                                homeowner.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">See All Options</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>
    <section class="hero grid-container " style="display: none;">
        <div class="hero-body text-hero">

            <div class="container  Account-Detail-content-title">





            </div>
        </div>
    </section>




</main>
@endsection
