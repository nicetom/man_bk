@extends('includes.master')

@section('content')
<div id="content" class="sixteen columns">

    <div id="post-147" class="post-147 page type-page status-publish hentry">


        <h1 class="entry-title">Share Savings</h1>


        <div class="entry-content">
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper"></div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <p>The Share Savings&nbsp;account represents your membership with the
                                        credit union. Once this account is established, you can enjoy the
                                        many other services and accounts available through your credit
                                        union.</p>
                                    <ul>
                                        <li>Open with a nominal $5 deposit – this is maintained for the life
                                            of your membership.</li>
                                        <li>Dividends are paid quarterly on the last day of March, June,
                                            September, and December.</li>
                                        <li>Statements are mailed quarterly.</li>
                                        <li>Setup automatic deposits into this account and watch your
                                            savings grow.</li>
                                        <li>Insured by the National Credit Union Administration</li>
                                    </ul>
                                    <h3></h3>
                                    <p></p>
                                </div>
                                <p></p>
                            </div>
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_wrapper_inner wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <h3><span style="color: #000080;">Share Savings Rates</span></h3>
                                    <table border="0" width="100%" cellspacing="0" cellpadding="3"
                                        align="left">
                                        <tbody>
                                            <tr>
                                                <td>.10% – Dividend</td>
                                                <td>.10%&nbsp;– APY*</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>*APY stands for Annual Percentage Yield.</p>
                                    <p></p>
                                </div>
                                <p></p>
                            </div>
                            <div
                                class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey">
                                <span class="vc_sep_holder vc_sep_holder_l"><span
                                        class="vc_sep_line"></span></span><span
                                    class="vc_sep_holder vc_sep_holder_r"><span
                                        class="vc_sep_line"></span></span>
                            </div>
                            <div class="vc_btn3-container vc_btn3-inline"><a
                                    class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-success"
                                    href="https://neafcu.org/neafcu/neafcu-rates-overview/"
                                    title="NEAFCU Rates Overview">View All Rates</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <div id="spoon-plugin-kncgbdglledmjmpnikebkagnchfdehbm-2"
                                        style="display: none;"></div>
                                    <p></p>
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div id="nav-below" class="navigation"></div><!-- #nav-below -->
        </div><!-- .entry-content -->
    </div><!-- #post-## -->

    <div id="comments">

        <!-- You can start editing here. -->



    </div>

</div><!-- /.columns (#content) -->
</div>
@endsection
