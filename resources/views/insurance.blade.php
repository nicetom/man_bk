@extends('includes.master')

@section('content')
<main id="content" class=" cards background-6">

    <section class="hero grid-container " >
        <div class="hero-body text-hero">

            <div class="container text-centered">
                <h1 class="title is-1 is-spaced reduced-margin text-maroon">
                    Personal Insurance Options
                </h1>
            </div>

        </div>
    </section>




    <section class="promo-banner background-color-red " >

        <div class="promo-image center-left hidden-xs-down"
            style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-12.jpg?ext=.jpg')">
        </div>



        <div class="promo-image background-image  hidden-xs-up"
            style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-12-small.jpg?ext=.jpg')">
        </div>


        <div class="promo-content contactus-banner">
            <h3 class="title is-3 white">Make sure you’ve<br>protected what<br>you’ve created for your&nbsp;family.</h3>
            <p class="promo-p hidden-lg-down white">

            </p>

        </div>




    </section>

    <section class="hero grid-container "  style="display: none;">
        <div class="hero-body text-hero">

            <div class="container text-centered">

            </div>

        </div>
    </section>


    <section class="creditCard-section " >

        <div class="grid-container">
            <div class="columns is-multiline card-columns">


                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile "
                    >
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/loan-insurance.svg?ext=.svg"
                                        alt="Loan &amp; Line of Credit Insurance">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Loan &amp; Line of Credit Insurance
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p style="text-align: center;">Buying a new vehicle or starting a home renovation?
                                &nbsp;Insurance can cover payments if you get disabled, or pay off your balance if you
                                become critically ill or pass away.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">Learn More</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile "
                    >
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/mortgage-insurance.svg?ext=.svg"
                                        alt="Mortgage Insurance">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Mortgage Insurance
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p style="text-align: center;">Your home is one of your biggest investments. Protect it with
                                mortgage insurance.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">Learn More</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile "
                    >
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/home-dark.svg?ext=.svg"
                                        alt="Home Insurance">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Home Insurance
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p style="text-align: center;">You’ve made your house a home. Protect everyone and
                                everything in it in case of flood, fire, or other natural disasters.</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">Learn More</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>
                <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile" >
                    <div class="card  ">

                        <div class="card-content card-header account-card-header">
                            <div class="media account-card-media">
                                <div class="media-image">
                                    <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/travel-insurance.svg?ext=.svg"
                                        alt="Travel Insurance">
                                </div>
                                <div class="media-content">
                                    <h4 class="title is-6 card-title">
                                        Travel Insurance
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="card-content card-body small-body Account-Detail-content-title">
                            <p style="text-align: center;">Anything can happen while you’re away on vacation. Our travel
                                insurance provides health coverage to protect you anytime, anywhere.*</p>

                        </div>

                        <div class="card-content card-footer account-card-footer centered">

                            <a class="cta-link fill-primary" href="#">
                                <div class="cta-wrapper">
                                    <span class="cta-text">Learn More</span>
                                    <div class="cta-img dark-arrow"></div>
                                </div>
                            </a>

                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>
    <section class="section-spacing mortgage-section Account-Detail-content-title" style="display: none;">
        <div class="grid-container " data-aos="fade-up" data-aos-duration="1000"
            data-aos-delay="350">

            <div class="mortgage-wrapper" style="Width:100%;">
                <div class="description">





                </div>
            </div>

        </div>
    </section>



    <section class="terms-and-conditions " >
        <div class="grid-container">
            <div class="viewport-spacing">
                <p>Loan / line of credit / mortgage insurance coverage is optional and is underwritten and provided by
                    CUMIS Life Insurance Company. &nbsp;Coverage is governed by the terms and conditions of the creditor
                    group insurance policy issued to the creditor and is subject to terms, conditions, exclusions and
                    eligibility requirements. &nbsp;See the <a href="#"
                        target="_blank">Product Guide and Certificate of Insurance</a> for full coverage
                    details.&nbsp;CUMIS® is a trademark of CUMIS Insurance Society, Inc.<br>
                    <br>
                    *Travel insurance does not cover everything. Please refer to the certificate for full terms and
                    conditions, including limitations and exclusions. Travel insurance is underwritten by CUMIS General
                    Insurance Company, a member of The Co-operators group of companies, and administered by Allianz
                    Global Assistance. Allianz Global Assistance is the registered business name of AZGA Service Canada
                    Inc. and AZGA Insurance Agency Canada Ltd.</p>





            </div>
        </div>
    </section>

</main>
@endsection
