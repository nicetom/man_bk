@extends('includes.master')

@section('content')
<main id="content" class="Account-Detail cards background-6">

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
      <div class="hero-body text-hero">

        <div class="container text-centered">
          <h1 class="title is-1 is-spaced reduced-margin text-maroon">
            </h1><h1>Personal Loans &amp; Lines of Credit</h1>











        </div>

      </div>
    </section>

    <section class="promo-banner background-color-red aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

    <div class="promo-image center-top hidden-xs-down" style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-9.jpg?ext=.jpg')"></div>



    <div class="promo-image background-image center-top hidden-xs-up" style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/Call-out-banner-9.jpg?ext=.jpg')"></div>


    <div class="promo-content contactus-banner">
      <h3 class="title is-3 white">Local decision making, competitive rates, and flexible repayment options.</h3>
      <p class="promo-p hidden-lg-down white">

      </p>

      <a class="cta-link fill-primary" href="https://www.scudirect.ca/LoanApplications?utm_source=scu-site&amp;utm_medium=promo-banner&amp;utm_campaign=loans-loc&amp;utm_content=personal-loans-loc-button" target="_blank">
        <div class="cta-wrapper">
          <span class="cta-text ">Apply Now</span>
          <div class="cta-img dark-arrow">
          </div>
        </div>
      </a>

    </div>




  </section>

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
      <div class="hero-body text-hero">

        <div class="container text-centered">
         <h2 style="text-align: center;">Loan or Line of Credit? Which is right for you?</h2>





        </div>

      </div>
    </section>


    <section class="creditCard-section aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

      <div class="grid-container">
        <div class="columns is-multiline centered">


  <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/borrowing-dark.svg?ext=.svg" alt="Loan">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Loan
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members who know how much money they’ll need for a specific purchase, such as buying a vehicle, paying for tuition, or consolidating high-interest debt.</p>

  <ul>
      <li>
      <p>Borrow a lump sum of money for a one‑time need</p>
      </li>
      <li>
      <p>Speak to a lending specialist to assess your specific needs</p>
      </li>
      <li>
      <p>Choose terms that work best for you with a set timeline to repay</p>
      </li>
  </ul>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href="/personal/borrowing/loans">
          <div class="cta-wrapper">
            <span class="cta-text">See All Loans</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div><div class="column auto-column is-4-desktop is-6-tablet is-12-mobile aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="card  ">

      <div class="card-content card-header account-card-header">
        <div class="media account-card-media">
          <div class="media-image">
            <img class="image" src="{{url('/')}}/assets/main/SCU/media/Images/line-of-credit.svg?ext=.svg" alt="Line of Credit">
          </div>
          <div class="media-content">
            <h4 class="title is-6 card-title">
              Line of Credit
            </h4>
          </div>
        </div>
      </div>

      <div class="card-content card-body small-body Account-Detail-content-title">
        <p>Best for members who want a flexible borrowing option. Tackle those home renovations, cover unexpected car repairs or emergency home maintenance.</p>

  <ul>
      <li>
      <p>Apply once for a pre-set maximum credit, then use money as you need it</p>
      </li>
      <li>
      <p>Pay interest only on what you use</p>
      </li>
      <li>
      <p>Rates lower than loans or credit&nbsp;cards</p>
      </li>
  </ul>

      </div>

      <div class="card-content card-footer account-card-footer centered">

        <a class="cta-link fill-primary" href=" /personal/borrowing/lines-of-credit">
          <div class="cta-wrapper">
            <span class="cta-text">See All Lines of Credit</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>

      </div>

    </div>
  </div>

        </div>
      </div>

    </section>
    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350" style="display: none;">
      <div class="hero-body text-hero">

        <div class="container  Account-Detail-content-title">





        </div>
      </div>
    </section>

    <section class="promo-banner background-color-maroon aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">

    <div class="promo-image center-top hidden-xs-down" style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/2020_02_ASAPP-Lending_WebImage_Hero.jpg?ext=.jpg')"></div>



    <div class="promo-image background-image center-top hidden-xs-up" style="background-image: url('{{url('/')}}/assets/main/SCU/media/Images/2020_02_ASAPP-Lending_WebImage_380x530.jpg?ext=.jpg')"></div>


    <div class="promo-content contactus-banner">
      <h3 class="title is-3 white">Get approved online with our new e-Loan</h3>
      <p class="promo-p hidden-lg-down white">
        Borrow up to $35,000 online with flexible repayment options.
      </p>

      <a class="cta-link fill-primary" href="https://eloan.scu.mb.ca/" target="_blank">
        <div class="cta-wrapper">
          <span class="cta-text ">Learn more</span>
          <div class="cta-img dark-arrow">
          </div>
        </div>
      </a>

    </div>




  </section>


  </main>
@endsection
