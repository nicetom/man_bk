@extends('includes.master')

@section('content')
<main id="content" class="background-6">

    <section class="hero grid-container aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
      <div class="hero-body text-hero">

        <div class="container text-centered Account-Detail-content">
          <h1 class="title is-1 is-spaced reduced-margin text-maroon">
            <strong>Wealth Management</strong>




          </h1>
          <p>Secure your future and protect your wealth with expert financial advice for every step in your journey.</p>





        </div>

      </div>
    </section>




    <section class="section-spacing Account-Detail-content-title aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
      <div class="grid-container standard-content double-content-section">
        <div class="columns">
          <div class="column is-6 content-section content-article">
            <h2><img alt="" src="{{asset('assets/main/SCU/media/Images//content-advisor.jpg')}}"></h2>

  <ul>
  </ul>





          </div>
          <div class="column is-6 content-section content-article">
            <h3>Our Approach to Wealth Management</h3>

  <p>We believe every person can benefit from working with a financial advisor, whether you're just starting out, building your wealth, or planning to leave a legacy. That's why we've chosen to partner with <a href="#" target="_blank">Credential®</a> - to leverage the experience and know-how of a team that provides superior wealth management expertise.<br>
  <br>
  What future are you working toward? What's important to you, now and in your future? Working with our team of wealth advisors and insurance specialists, you'll explore the answers to these questions, and together, you'll build solutions to help you reach your goals.<br>
  &nbsp;</p>

  <h4>Find your way to wealth in five easy steps:</h4>
  &nbsp;

  <ol>
      <li><strong>Meet with us</strong>: Your wealth management journey starts with a meeting where we discuss your goals and dreams.</li>
      <li><strong>Analyze your situation</strong>: We’ll review everything that makes up your current financial picture and see where you stand financially, right now.</li>
      <li><strong>Develop your financial plan</strong>: We’ll create a financial plan with strategies and solutions to help you reach your goals, build your wealth, and protect what matters.</li>
      <li><strong>Implement your plan</strong>: Once you approve your plan, we’ll implement it.</li>
      <li><strong>Review, monitor and evaluate</strong>: Our team will continue to monitor and fine-tune your strategy to keep you on track.</li>
  </ol>





          </div>
        </div>
      </div>
    </section>
  <section class="section-spacing mortgage-section Account-Detail-content-title">
      <div class="grid-container">
        <div class="mortgage-wrapper" style="Width:100%;">
          <div class="description">
            <h3>Our Wealth Services Include:</h3>

  <ul>
      <li>Savings and investment strategies</li>
      <li>Comprehensive retirement planning</li>
      <li>Portfolio monitoring and re-balancing</li>
      <li>Tax-efficient investing</li>
      <li>Family wealth management</li>
      <li>Personal and business insurance planning</li>
      <li>Business succession consultation</li>
  </ul>










          </div>

        </div>
      </div>
    </section>
     <section class="section-spacing Account-Detail-content-title" style="display: none;">
      <div class="grid-container standard-content double-content-section">
        <div class="columns">
          <div class="column is-6 content-section content-article">





          </div>
          <div class="column is-6 content-section content-article">





          </div>
        </div>
      </div>
    </section>




     <section class="section-spacing Account-Detail-content-title" style="display: none;">
      <div class="grid-container standard-content double-content-section">
        <div class="columns">
          <div class="column is-6 content-section content-article">





          </div>
          <div class="column is-6 content-section content-article">





          </div>
        </div>
      </div>
    </section>




    <section class="promo-banner background-color-maroon ">

    <div class="promo-image center-top hidden-xs-down" style="background-image: url({{asset('assets/main/SCU/media/Images/Call-out-banner-6.jpg?ext=.jpg')}})"></div>



    <div class="promo-image background-image center-top hidden-xs-up" style="background-image: url({{asset('assets/main/SCU/media/Images/Call-out-banner-6-small.jpg?ext=.jpg')}})"></div>


    <div class="promo-content contactus-banner">
      <h3 class="title is-3 white">Start the Conversation</h3>
      <p class="promo-p hidden-lg-down white">
        It all starts with a simple conversation. No matter where you are in your journey, we'll connect you with a financial advisor who understands your needs.&nbsp;
      </p>

      <a class="cta-link fill-primary" href="#">
        <div class="cta-wrapper">
          <span class="cta-text ">Let us contact you </span>
          <div class="cta-img dark-arrow">
          </div>
        </div>
      </a>

    </div>




  </section>

      <section class="terms-and-conditions aos-init aos-animate" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="350">
    <div class="grid-container">
      <div class="viewport-spacing">
        <p>*Mutual funds and related financial planning are offered through Credential Asset Management Inc.&nbsp;<br>
  Mutual funds and other securities &nbsp;and securities related financial planning are offered through Credential Securities, a division of Credential Qtrade Securities Inc. &nbsp;Credential Securities is a registered mark owned by Aviso Wealth Inc.</p>





      </div>
    </div>
  </section>

  </main>
@endsection
