<script type="text/javascript">
  function initMap() {
    const event = new CustomEvent('onMapLoad');
    window.dispatchEvent(event);
  }
</script>
<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaAVCR9BWRKb8ihE9Kttaa1B4KrCgH6vs&amp;libraries=places,geometry&amp;callback=initMap"
></script>
<script src="{{asset('assets/main/unpkg.com/aos%402.3.1/dist/aos.html')}}"></script>

<script>
  AOS.init({
    once: true
  });
</script>

<script type="text/javascript" src="{{asset('assets/main/CMSScripts/Custom/bundle.js')}}"></script>





<script type="text/javascript">
	//<![CDATA[
WebServiceCall('/cmsapi/webanalytics', 'LogHit', '{"NodeAliasPath":"/Personal-Banking","DocumentCultureCode":"en-CA","UrlReferrer":""}')
//]]>
</script>
<script type="text/javascript">
//<![CDATA[

var callBackFrameUrl='WebResourceaf1f.html?d=beToSAE3vdsL1QUQUxjWdcJVkeBk7R0u8VFmMsq8nvPFeDGKTqkHI50r3TIePfmqhq-VjCVnCkAb4w9E_CtdUQ2&amp;t=636426639113374701';
WebForm_InitCallback();//]]>
</script>

<script type="text/javascript">
	//<![CDATA[

var items = document.getElementsByClassName("nav-banking-toggle");
for (i = 0; i < items.length; i++) {
  if(items[i].innerText == "Personal Banking")
     items[i].classList.add("active");
}
//]]>
</script>
<script type="text/javascript">
	//<![CDATA[
document.getElementsByClassName("searchbar")[0].setAttribute("aria-label", "Search this site");
document.getElementsByClassName("searchbar-input")[0].setAttribute("aria-label", "Stay informed with our newsletter");
//document.getElementsByClassName("searchbar-input")[1].setAttribute("aria-label", "Stay informed with our newsletter");

//]]>
</script>
<script type="text/javascript">
	//<![CDATA[
document.getElementsByClassName("searchbar")[0].setAttribute("onkeypress", "handle(event)");
function handle(e) {
    if (e.keyCode === 13) {
        e.preventDefault(); // Ensure it is only this code that rusn
        const button = document.querySelector("input[type='image'][id*='SmartSearchBox2_btnImageButton']");
        window.location.href = '/Search-Result?searchtext=' + document.getElementsByClassName("searchbar")[0].value + '&searchmode=allwords'
        return false;
    }
}
//]]>
</script><!--Intro JS-->
<script src="{{asset('assets/main/cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js')}}"></script>

<script>
var items = document.getElementsByTagName("section");
for (i = 0; i < items.length; i++) {
if(items[i].innerText == "" && items[i].getAttribute("class").indexOf('full-image-banner')==-1)
   items[i].style.display = "none";
}

</script>
<script type="text/javascript">
//<![CDATA[

theForm.oldSubmit = theForm.submit;
theForm.submit = WebForm_SaveScrollPositionSubmit;

theForm.oldOnSubmit = theForm.onsubmit;
theForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;
Sys.Application.add_init(function() {
    $create(Sys.Extended.UI.TextBoxWatermarkBehavior, {"ClientStateFieldID":"p_lt_ctl05_SmartSearchBox1_txtWord_exWatermark_ClientState","id":"p_lt_ctl05_SmartSearchBox1_txtWord_exWatermark","watermarkText":"Search"}, null, null, $get("p_lt_ctl05_SmartSearchBox1_txtWord"));
});
Sys.Application.add_init(function() {
    $create(Sys.Extended.UI.TextBoxWatermarkBehavior, {"ClientStateFieldID":"p_lt_ctl08_SmartSearchBox2_txtWord_exWatermark_ClientState","id":"p_lt_ctl08_SmartSearchBox2_txtWord_exWatermark","watermarkText":"Search"}, null, null, $get("p_lt_ctl08_SmartSearchBox2_txtWord"));
});
Sys.Application.add_init(function() {
    $create(Sys.Extended.UI.TextBoxWatermarkBehavior, {"ClientStateFieldID":"p_lt_ctl20_On_lineForm_viewBiz_Email_exWatermark_ClientState","id":"p_lt_ctl20_On_lineForm_viewBiz_Email_exWatermark","watermarkCssClass":"form-control searchbar-input informed-input WatermarkText","watermarkText":"Email"}, null, null, $get("p_lt_ctl20_On_lineForm_viewBiz_Email_txtText"));
});
//]]>
</script>
