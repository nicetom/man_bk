<nav class="nav-bottom">
    <ul class="bottom-nav-wrapper">
        <li class="nav-banking-list">
            <a class="nav-banking-li " href="{{route('template',['accounts'])}}" aria-haspopup="true" aria-expanded="false"
                data-for="subnav-accounts">
                <span>Accounts</span>
            </a>

        </li>
        <li class="nav-banking-list">
            <a class="nav-banking-li " href="{{route('template',['investing'])}}" aria-haspopup="true" aria-expanded="false"
                data-for="subnav-investing">
                <span>Investing</span>
            </a>
        </li>

        <li class="nav-banking-list">
            <a class="nav-banking-li " href="{{route('template',['mortgage'])}}" aria-haspopup="true" aria-expanded="false"
                data-for="subnav-mortgages">
                <span>Mortgages</span>
            </a>
        </li>

        <li class="nav-banking-list">
            <a class="nav-banking-li " href="{{route('template',['borrowing'])}}" aria-haspopup="true" aria-expanded="false"
                data-for="subnav-borrowing">
                <span>Borrowing</span>
            </a>
        </li>


        <li class="nav-banking-list">
            <a class="nav-banking-li " href="{{route('template',['insurance'])}}" aria-haspopup="true" aria-expanded="false"
                data-for="subnav-insurance">
                <span>Insurance</span>
            </a>
        </li>
    </ul>
</nav>
