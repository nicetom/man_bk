<footer class="footer">
    <div class="grid-container relative">

        <nav class="columns">
            <a class="column is-2 hidden-md-down" href="{{url('/')}}"><img alt="SCU"
                    src="{{asset('assets/main/login/images/CTZ_Green-01.png')}}" />



            </a>

            <div class="column is-2 is-offset-1 is-6-mobile is-offset-0-mobile footer-grouping padding-L">
                <ul>
                    <li><a href="#"><span>Annual Reports</span></a></li>
                    <li><a href="#"><span>Board of Directors</span></a></li>
                    <li><a href="#"><span>Executive Leadership</span></a></li>

                </ul>

            </div>

            <div class="column is-2 is-6-mobile footer-grouping padding-L">
                <ul>
                    <li><a href="#"><span>Insights &amp; Advice</span></a></li>
                    <li><a href="#"><span>{{config('app.name')}}Member Newsletter</span></a></li>
                    <li><a href="#"><span>Media Centre</span></a></li>
                    <li><a href="#" target="_blank"><span>Frequently Asked
                                Questions</span></a></li>
                    <a name="enews-signup"> </a>
                </ul>

            </div>


        </nav>

        <div class="columns">
            <div class="column is-offset-3 is-8 footer-base padding-LR">
                <p class="footer-heading">Stay informed with our e-news



                </p>
                <div class="signup-section">

                    <div class="scu-searchbar">
                        <div id="p_lt_ctl20_On_lineForm_viewBiz">
                            <div id="p_lt_ctl20_On_lineForm_viewBiz_pM_pMP">

                            </div>
                            <div id="p_lt_ctl20_On_lineForm_viewBiz_pnlForm" class="FormPanel"
                                onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;p_lt_ctl20_On_lineForm_viewBiz_btnOK&#39;)">
                                <div class="scu-searchbar" id="input-scu-searchbar">
                                    <div id="p_lt_ctl20_On_lineForm_viewBiz_ncpemail"
                                        class="EditingFormControlNestedControl editing-form-control-nested-control">
                                        <input name="p$lt$ctl20$On_lineForm$viewBiz$Email$txtText" type="text"
                                            maxlength="200" id="p_lt_ctl20_On_lineForm_viewBiz_Email_txtText"
                                            class="form-control searchbar-input informed-input" />
                                        <input type="hidden"
                                            name="p$lt$ctl20$On_lineForm$viewBiz$Email$exWatermark_ClientState"
                                            id="p_lt_ctl20_On_lineForm_viewBiz_Email_exWatermark_ClientState" />
                                    </div> <input type="submit" name="p$lt$ctl20$On_lineForm$viewBiz$btnOK"
                                        value="Sign Up" id="p_lt_ctl20_On_lineForm_viewBiz_btnOK"
                                        class="FormButton btn btn-primary" />
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="footer-disclaimer">

                    <div class="disclaimer-links">
                        <ul>
                            <li><span><a href="#">Site Map</a>&nbsp;</span></li>
                            <li><span><a href="#">Legal</a></span><span>&nbsp;</span></li>
                            <li><span><a href="#">Security &amp;
                                        Privacy</a></span><span>&nbsp;</span></li>
                            <li><a href="#"><span>Accessibility</span></a></li>
                        </ul>





                    </div>

                    <p>&copy; 2019 {{config('app.name')}}. All Rights Reserved.</p>





                </div>

            </div>
        </div>

    </div>
</footer>
