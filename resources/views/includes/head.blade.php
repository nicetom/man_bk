<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head id="head">
    <title>
        {{config('app.name')}} - {{$title}}
    </title>
    <meta charset="UTF-8" />
    <!--Load Custom Style Start-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('assets/main/unpkg.com/aos%402.3.1/dist/aos.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/main/cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css')}}" />
    <link href="{{asset('assets/main/App_Themes/SCUStyle/styles.css')}}" rel="stylesheet">
    <link href="{{asset('assets/main/CMSPages/SCUStylea184.css?stylesheetname=SCUStyle')}}" type="text/css" rel="stylesheet" />
    <!--Load Custom Style Ends-->
    <link rel="shortcut icon" href="{{asset('assets/main/login/images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('assets/main/login/images/favicon.ico')}}" type="image/x-icon">
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                                      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '{{url('/')}}/assets/main/www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
                            })(window,document,'script','dataLayer','GTM-W4R9FKD');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W4R9FKD" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-12159399-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-12159399-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 854031736 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-854031736"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-854031736');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
     if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
     n.queue=[];t=b.createElement(e);t.async=!0;
     t.src=v;s=b.getElementsByTagName(e)[0];
     s.parentNode.insertBefore(t,s)}(window, document,'script',
                                     '{{url('/')}}/assets/main/connect.facebook.net/en_US/fbevents.js');
  fbq('init', '343394362775494');
  fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
            src="{{url('/')}}/assets/main/www.facebook.com/trc244.gif?id=343394362775494&amp;ev=PageView&amp;noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->


    <!-- SET: Open Graph -->
    <meta property="og:site_name" content="{{config('app.name')}}" />
    <meta property="og:url" content="{{url('/')}}">

    <link href="{{asset('assets/main/CMSPages/SearchBoxb32c.css?_webparts=677')}}" type="text/css" rel="stylesheet" />
    <link href="{{asset('assets/main/CMSPages/SearchBox_SCUSearchLayout855a.css?_webpartlayouts=651')}}" type="text/css" rel="stylesheet" />
</head>
