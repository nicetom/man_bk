@include('includes.head')

<body class="LTR Mozilla ENCA ContentBody">

    <form method="post" action="https://www.scu.mb.ca/" onsubmit="javascript:return WebForm_OnSubmit();" id="form">
        <div class="aspNetHidden">
            <input type="hidden" name="__CMSCsrfToken" id="__CMSCsrfToken"
                value="AQ+pnN1Jo19VIhd+b+ch7RojmUw1xM5ClVceHWcqIbq771COKBkzIGDPl062FBlcaaTncRWaL/K3jXdiyCVdyj9bzFgwVbLD7ymhZhnxYAw=" />
            <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
            <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE"
                value="yL3KU9lr14OsEu7oTd/86kaEKNq/Ld0VtYwnEkqJxn2Xhc6NXL1zx5s19FDQVQpoGxP+FtB8s1P/K7ZdrVhzdPXuMfn2iPXsXtIdjGsH7ES7Bxwli2rsPMfUNgy+9N9+zFznKJBqN7LjRb8h/VzTWWMwudfhBY9C0SV+ztlJg60HhQmk9lGrt2bvwIKxU1vTvXU/96UFPrDrSv9Hg08F3FmqCe99gxO4JMGZa1wn8Du/Gw6lJcJRVQ3RG06Utfuas536YYRV9BYvlla8Yhed0eb1fjYFfugswiA+HkkZQkWunWhpDklm/aFGx92e8+mw7WN621CLCO0I65WoQ8pJXDC6FC+ywlKY+44M3yZyPutc95TVN53WngPo+qKzB5ey13SdkL/M0XmbkKhhhSb768njOU1U81L4AzMQvRIefuaaOG/LG53xmoppeCSq9zjnrkBd8+mqYeyjMbI5teCCDbd9YvqYqR7jdPbX8J0o/S24bZ67J21oIyvTl3ua3FE0p1ynjkH+ADS60lclMrOWWUuOfgOxG8AhseakyEd7ImM3btPklWFvJ2UGODmj71HiKMxj5MywyZQGFtM1TTL69NqyNwj13cMdNxMj+WgNBnltdfCXupUD3MZxMOCBYrmmpKzuwCIBZH84P1yV2eFbdmqvYKT1B+NW8DliHH2uCnIeYWt5pB+BfLEE6Hyiq98TYh7by6RzphOwVuHLWlyy1pwOWwgCU1WIjT1vyNsauxdinoIx05tVDlHg41LhcJtmdqEWatoei1Ce2ZbT0n4yzHqe3p/z6aejqmfMR+Z+6yH1mkOecYwgAPjxZrmjm/t8rgiYwZAcGnsC6SCVd0334cVw1lYae1AKkieuKc8vUGDulAng7ETKRtRjlEaBOkVsGrBadv31la0OoG4PYrQ7C62V9l7dA/uEq+MJRZ9HuKlAw6+Z6Ta3qALG5QQtnPZKacf8Iw8CK0i0JTEmAeHZaLw9lfmjC2I1pkm/5dpW2lQD93bjLTwdD6Lc7M0X923yFM16ZAwaWKL/yoK8bl4hcd+cbl9pPmTGojJNNhkLnzyhkV8jRvgtNkZHZnjp6Go6qpeUrbFsrZYJoC/Rj1wNW4hoGrLcz4l1G7etsmABFiU2mZU2WKHLTFfMKhnhNU8D8orb57/3SN6BgGSZT5UBoc/9qW6weG4eu3sdqASYAgC+h8w999KG2aGFvINoaaQJcBA/Ev8KRVr9peW7vRRK9ifJIeuGrdh2Tt100qzG+oFqcvsLEb+IKfa2pHGDaIDWsN35RIw2MelknAUjj7CKbcx+dqb8IUOq9AyqADMJeVqVU2dkprBNUhasGkE+cIyw/lTNEn6HuDm+/0Rbay3iplgxysEvJ8jNF0+oftzNrSrfV5Iee/BUms9A1AlTpi64kBLCV7jy4GoGrLMRYUL4VkKqSsO+tbaEF7GznUd+HFvN6oSQRc45lI1iR+iYnkIBhVKapoTVXlhsIT2TlgP8X2kRmr+fO6u142wIs33izs/jO+z1+m0D/8ULJf/Ox2SiY3hl6SqRtURi2Ri8WuZueqQ67++Mn78tWOUOSIWsllaDwRqBcWsNZzurNxp2e2dtG9b7y+9rVfJEWqKOhqLA16QUqokaS9kkFAhvhT9dIF7fWRnUu2CjlbNCFB6F/IBXqHKSVtz6CdO6HsEnwAYptvjfXwRtpj8n9lsgASrT9cv0otZJLjBAwfDtmwVGGM18y4xiKNEDrshhS/gn+dnELgQtgXQpRG9MSp9vBx+3MDCIMG59fhGp3VX5Bz+zS2hxH7IixKuX6sQsN4rBi2MdGSmAuSXBq1GFFR5948eK4z5XlymBN35kG7ceWID+l1hY6DAIKHzgwJqbfibOQeiKXllgI87TCGegiVDenhTQ+Og7oujLpbSmONCAbAbKbCAhivoywoKyp0l8CwknlyGcCk8Zk1v62hGhKwubodVi48tRZ/27DSjnnCwNofIt7IG1VpD7NZjwgVoE8pY/+Em3kWQs0jL2+y5ZhjfXD4AyAlH7DxfE0MLo6tuKAzo+yCGcSZ3qvzw/D/2bsOclpAsiK3apALecFUuz7akYf7lvqZ3i5UQC/iAMws56rgPEB3Z4IOeYO+LRtaQHj9Cjr3FOsidhKHGoHztaJiecs29rTe3MxJadEzHYTYVMx9T/AarzPAlVbbTI49cALceWrkYlx/tYncTIE3KGAVWpM5vYR0xQltiVr+yJBYBeIhlv3wxH" />
        </div>

        <script type="text/javascript">
            //<![CDATA[
var theForm = document.forms['form'];
if (!theForm) {
    theForm = document.form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
        </script>


        <script
            src="{{url('/')}}/assets/main/WebResource7fb7.js?d=pynGkmcFUV13He1Qd6_TZAowrydZVLWyY1j8SzNkbnjzxZP_BHe_SRrPy5cIBllNjwkrSyyBRTZoUqzDs9Bqbw2&amp;t=636426639113374701"
            type="text/javascript"></script>

        <input type="hidden" name="lng" id="lng" value="en-CA" />
        <script src="{{url('/')}}/assets/main/CMSPages/WebServiceCall2bfa.js?scriptfile=%7e%2fCMSScripts%2fWebServiceCall.js"
            type="text/javascript"></script>
        <script type="text/javascript">
            //<![CDATA[

function PM_Postback(param) { if (window.top.HideScreenLockWarningAndSync) { window.top.HideScreenLockWarningAndSync(1080); } if(window.CMSContentManager) { CMSContentManager.allowSubmit = true; }; __doPostBack('m$am',param); }
function PM_Callback(param, callback, ctx) { if (window.top.HideScreenLockWarningAndSync) { window.top.HideScreenLockWarningAndSync(1080); }if (window.CMSContentManager) { CMSContentManager.storeContentChangedStatus(); };WebForm_DoCallback('m$am',param,callback,ctx,null,true); }
//]]>
        </script>
        <script
            src="{{url('/')}}/assets/main/ScriptResource4e23.js?d=NJmAwtEo3Ipnlaxl6CMhvt-haXELiM_hyh-Xe9NZYku7YFFY60yJs_uk4j_LED57QVFZmIJjS9yra69QcvCxeJWuRUCF3FwWDi41iveNEe8ZY7W-F6fNdCqtNiOEuw2CI3xpl8btTqsfpzGDEWiF2FhQyl7P2q5v4AXugTUyt4s1&amp;t=72fc8ae3"
            type="text/javascript"></script>
        <script
            src="{{url('/')}}/assets/main/ScriptResource9541.js?d=dwY9oWetJoJoVpgL6Zq8OCLyBz82CCzOL4FMmiW68Xm0_oktji435CNX6m_PjgFPo3gQUUnX8XDSJnnOS6R8TlOXG9tEzWnHLEJ-rngISS-guMAq0TlKNECoH2JLS0mIoel5L-xKA22vrgdfpfuW4w0EjgwP7K5H52pPrnVv8sc1&amp;t=72fc8ae3"
            type="text/javascript"></script>
        <script
            src="{{url('/')}}/assets/main/ScriptResourcea12a.axd?d=eE6V8nDbUVn0gtG6hKNX_IRxXhN6oa5kgN5E_tg7JwtovIiCMnz373xAwHqQ1VrTs3Gu7IF00uWW_HBo-uwIot_a9u5eVQOpg9qCEyF4UdhYO8kbcFrm6Yl-jTzSGEEU0&amp;t=27679117"
            type="text/javascript"></script>
        <script
            src="{{url('/')}}/assets/main/ScriptResource2b85.axd?d=mcAASOSteirJeuV-3by3UUvBUEEjvYA5rYKYcem7auBAZg1rdxlzxFHKk5XTyRv0MdpU9F5-lr_Ks3eWQqBjzJOrCRgoOMl7aMenLWJL3dE1&amp;t=27679117"
            type="text/javascript"></script>
        <script
            src="{{url('/')}}/assets/main/ScriptResourcec766.axd?d=jwf4VSQi7LeShc44FJ-gAdXee9ARuithaAHCjfwY99hC5tZEL9Y72kgsj0wwijvdP5pZLy7u7gj9CKe9UYa8y2ccZihdA62941X8D9F601TtlXv1hn3eXmVJcVql05f80&amp;t=27679117"
            type="text/javascript"></script>
        <script
            src="{{url('/')}}/assets/main/ScriptResource4701.axd?d=HEFpVKbnoeQjkjHkFKu3MN4wlRMTRyMWNZulgWVNgV2Hbb3ppPT0haQqFMe_G__nhc9blLn1njpumukrXKFzngXRcdaPO5ac-NWNkJqOi6PxbXEnmNBhksfsfgzMbGr00&amp;t=27679117"
            type="text/javascript"></script>
        <script
            src="{{url('/')}}/assets/main/ScriptResource68eb.axd?d=X6kQKInQS5YQqruiTh57iNwLW7Xo0mpfR0q3_s6PlnnZ9pKnXmWfGKF74n82ZzfJTP8WQPRsdGhryTmyTfGGj9WFf_5Cn8UILmmY3xrxHDSgMNr5ZWtTiyACvJiN2I1X0&amp;t=27679117"
            type="text/javascript"></script>
        <script type="text/javascript">
            //<![CDATA[

var CMS = CMS || {};
CMS.Application = {
  "language": "en",
  "imagesUrl": "{{url('/')}}/CMSPages/GetResource.ashx?image=%5bImages.zip%5d%2f",
  "isDebuggingEnabled": false,
  "applicationUrl": "/",
  "isDialog": false,
  "isRTL": "false"
};

//]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
function WebForm_OnSubmit() {
null;
return true;
}
//]]>
        </script>

        <div class="aspNetHidden">

            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A5343185" />
            <input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
            <input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
        </div>
        <script type="text/javascript">
            //<![CDATA[
Sys.WebForms.PageRequestManager._initialize('manScript', 'form', ['tctxM','','tp$lt$ctl20$On_lineForm$viewBiz$pM$pMP',''], [], [], 90, '');
//]]>
        </script>

        <div id="ctxM">

        </div>

        <a href="#content" class="skip-to-content">Skip to Content</a>



        <header class="header relative  ">
            <div class="header-main">
                <div class="grid-container flex">

                    <div class="header-logo">
                        <a class="scu-logo-wrapper" href="{{url('/')}}" aria-label="{{config('app.name')}}">
                            <img id="p_lt_ctl01_EditableImage2_ucEditableImage_imgImage"
                                class="scu-logo logo-small header-small hidden-lg-up"
                                src="{{url('/')}}/assets/main/login/images/small_logo.png" alt="" />
                        </a>
                    </div>


                  @include('includes.navbar')

                </div>
            </div>

            <div class="grid-container nav-main" data-for="hamburger-menu">

                <div class="nav-search search-for-small">
                    <div id="p_lt_ctl05_SmartSearchBox1_pnlSearch" class="searchBox"
                        onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;p_lt_ctl05_SmartSearchBox1_btnImageButton&#39;)">

                        <label for="p_lt_ctl05_SmartSearchBox1_txtWord" id="p_lt_ctl05_SmartSearchBox1_lblSearch"
                            style="display:none;">Search for:</label>
                        <input type="hidden" name="p$lt$ctl05$SmartSearchBox1$txtWord_exWatermark_ClientState"
                            id="p_lt_ctl05_SmartSearchBox1_txtWord_exWatermark_ClientState" /><input
                            name="p$lt$ctl05$SmartSearchBox1$txtWord" type="text" maxlength="1000"
                            id="p_lt_ctl05_SmartSearchBox1_txtWord" class="nav-search-input form-control" />

                        <input type="image" name="p$lt$ctl05$SmartSearchBox1$btnImageButton"
                            id="p_lt_ctl05_SmartSearchBox1_btnImageButton" src="{{url('/')}}/assets/main/SCU/media/Images/icons/searchimg.png"
                            alt="Search" />
                        <div id="p_lt_ctl05_SmartSearchBox1_pnlPredictiveResultsHolder" class="predictiveSearchHolder">

                        </div>

                    </div>

                </div>

                <div class="header-sub flex ">

                    <a style="display:none">cms.document.cms.root (1)</a> <a
                        class="scu-logo-wrapper header-large hidden-lg-down" href="{{url('/')}}"><img
                            alt="SCU logo linking to website homepage" class="scu-logo header-large"
                            src="{{asset('assets/main/login/images/CTZ_Green-01.png')}}" style="width: 140px; height: 46px;" /></a>





                    <div class="header-sub-right flex">

                        <nav class="nav-banking flex">
                            <div class="nav-toggle" id="step-3">

                                <a class="nav-banking-toggle active" href="{{url('/')}}" data-click="nav">
                                    <span class="nav-banking-toggle-text">Personal Banking</span>
                                </a>
                                <a class="nav-banking-toggle " href="{{route('template',['business'])}}" data-click="nav">
                                    <span class="nav-banking-toggle-text">Business & Agriculture</span>
                                </a>
                                <a class="nav-banking-toggle " href="{{route('template',['wealth'])}}" data-click="nav">
                                    <span class="nav-banking-toggle-text">Wealth Management</span>
                                </a>


                            </div>


                            <div class="header-searchbar">
                                <div id="p_lt_ctl08_SmartSearchBox2_pnlSearch" class="smartSearch"
                                    onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;p_lt_ctl08_SmartSearchBox2_btnImageButton&#39;)">

                                    <label for="p_lt_ctl08_SmartSearchBox2_txtWord"
                                        id="p_lt_ctl08_SmartSearchBox2_lblSearch" style="display:none;">Search
                                        for:</label>


                                    <input type="image" name="p$lt$ctl08$SmartSearchBox2$btnImageButton"
                                        id="p_lt_ctl08_SmartSearchBox2_btnImageButton"
                                        src="{{url('/')}}/assets/main/SCU/media/Images/icons/searchimg.png" alt="Search" />
                                    <input type="hidden"
                                        name="p$lt$ctl08$SmartSearchBox2$txtWord_exWatermark_ClientState"
                                        id="p_lt_ctl08_SmartSearchBox2_txtWord_exWatermark_ClientState" /><input
                                        name="p$lt$ctl08$SmartSearchBox2$txtWord" type="text" maxlength="1000"
                                        id="p_lt_ctl08_SmartSearchBox2_txtWord" class="searchbar form-control" />
                                    <div id="p_lt_ctl08_SmartSearchBox2_pnlPredictiveResultsHolder"
                                        class="predictiveSearchHolder">

                                    </div>

                                </div>



                            </div>

                        </nav>

                        @include('includes.bottomnav')
                    </div>
                </div>

            </div>
        </header>



        @yield('content')

        @include('includes.footer')
        @include('includes.script')
    </form>


</body>


</html>
