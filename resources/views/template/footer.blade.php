<footer class="footer">
    <?php $year = (int)  date('Y',time());?>
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © {{$year}} <a disabled href="#" target="_blank">{{ config('app.app_name')}}</a>. All rights reserved.</span>
    </div>
  </footer>
