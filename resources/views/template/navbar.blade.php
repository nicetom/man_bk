<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    @if (Auth::check())
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{url('/')}}"><img src="{{asset('assets/main/login/images/CTZ_Green-01.png')}}" alt="logo" /></a>
        <a class="navbar-brand brand-logo-mini" href="{{url('/')}}"><img src="{{asset('assets/main/login/images/small_logo.png')}}" alt="logo" /></a>
      </div>
    @elseif(Auth::guard('admin')->check())
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
      <a class="navbar-brand brand-logo" href="{{route('admin_home')}}"><img src="{{asset('assets/main/login/images/CTZ_Green-01.png')}}" alt="logo" /></a>
      <a class="navbar-brand brand-logo-mini" href="{{route('admin_home')}}"><img src="{{asset('assets/main/login/images/small_logo.png')}}" alt="logo" /></a>
    </div>
    @endif
    <div class="navbar-menu-wrapper d-flex align-items-stretch">
      <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
        <span class="mdi mdi-menu"></span>
      </button>

      <ul class="navbar-nav navbar-nav-right">
        <li class="nav-item nav-profile dropdown">
          <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">

            @if (Auth::guard('admin')->check())

            <div class="nav-profile-img">
              <img src="{{asset('assets/images/faces/face1.jpg')}}" alt="image">
              <span class="availability-status online"></span>
            </div>
            <div class="nav-profile-text">
                <p class="mb-1 text-black">{{Auth::guard('admin')->user()->firstname}}, {{Auth::guard('admin')->user()->lastname}}</p>
            </div>
            @elseif(Auth::check())
            <div class="nav-profile-img">
                <img src="{{ asset('public/storage/profile_images')}}/{{Auth::user()->image->image_location}}" alt="image">
                <span class="availability-status online"></span>
              </div>
              <div class="nav-profile-text">
                  <p class="mb-1 text-black">{{Auth::user()->firstname}}, {{Auth::user()->lastname}}</p>
              </div>
            @endif
          </a>
          @if (Auth::guard('admin')->check())
          <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
            <a class="dropdown-item" href="{{route('admin_logout')}}">
              <i class="mdi mdi-logout mr-2 text-primary"></i> Logout </a>
          </div>
          @elseif(Auth::check())
          <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
            <a class="dropdown-item" href="{{route('logout')}}">
              <i class="mdi mdi-logout mr-2 text-primary"></i> Logout </a>
          </div>
          @endif

        </li>


      </ul>
      <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
    </div>
  </nav>
