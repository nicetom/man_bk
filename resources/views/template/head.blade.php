<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.app_name')}} - {{$title}}</title>
    <link rel="shortcut icon" href="{{asset('assets/main/login/images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('assets/main/login/images/favicon.ico')}}" type="image/x-icon">
    <meta name="base_url" content="{{url('/')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <style type="text/css">
        .money-credit {
            color: green;
        }

        .money-debit {
            color: red;
        }

        .middle {
            color: red;
            text-align: center;
        }

        .content-wrapper {
            background: #f2fbf7;
        }

        .tr-class {
            background: #58d29c;
        }

        .sidebar .nav .nav-item.active>.nav-link .menu-title {
            color: #58d29c;
        }

        .sidebar .nav .nav-item.active>.nav-link i {
            color: #58d29c;
        }

        .show{
            display: inline-block;
        }
        .hide{
            display: none;
        }
    </style>
    @yield('other_styles')
</head>
