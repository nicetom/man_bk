@include('template.head')

<body>
    <div class="container-scroller">
        @include('template.navbar')

        <div class="container-fluid page-body-wrapper">
            @include('template.sidebar')
            <div class="main-panel">
                <div class="content-wrapper">
                    @if(count($errors) > 0)
                    @include('template.errors')
                @endif
                    <div class="page-header">
                        <h3 class="page-title">
                            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                <i class="mdi mdi-home"></i>
                            </span> {{$title}} </h3>

                    </div>
                    @yield('content')
                </div>
                @include('template.footer')

            </div>
        </div>
    </div>
    <div class="modal fade" id="popupmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deleteLabel">Message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p>
                    We are currently Experiencing some challenges with some functionalities, please bear with us as our team of developers are working to fix the problem.
                    <br><br> Thanks for Understanding
                </p>
              </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    @include('template.scripts')
</body>

</html>
