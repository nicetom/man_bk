@foreach($errors->all() as $error => $message)
    @unless($error != 'tries')
		@if(Session::has('green'))
		<div class ="alert alert-success alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<center>{{$message}}</center>
		</div>
		@elseif(Session::has('red'))
		<div class ="alert alert-danger alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<center>{{$message}}</center>
        </div>
        @elseif(Session::has('blue'))
		<div class ="alert alert-info alert-dismissable" >
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<center>{{$message}}</center>
		</div>
        @endif
        @endunless
	@endforeach
	<?php Session::forget('red'); Session::forget('green'); Session::forget('blue'); ?>
