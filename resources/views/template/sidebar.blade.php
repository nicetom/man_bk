<nav class="sidebar sidebar-offcanvas" id="sidebar">
    @if (Auth::guard('admin')->check())
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">

                <div class="nav-profile-image">
                    <img src="{{asset('assets/images/faces/face1.jpg')}}" alt="profile">
                    <span class="login-status online"></span>
                </div>
                <div class="nav-profile-text d-flex flex-column">
                    <span class="font-weight-bold mb-2">{{Auth::guard('admin')->user()->firstname}},
                        {{Auth::guard('admin')->user()->lastname}} </span>
                    <span class="text-secondary text-small">Administrator</span>
                </div>


                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('admin_home')}}">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-bank menu-icon"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">User Management</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-worker menu-icon"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('create_user')}}">Create User</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('users')}}">View Users</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('allocate_accounts')}}">
                <span class="menu-title">Allocate Accounts</span>
                <i class="mdi mdi-contacts menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-transaction" aria-expanded="true"
                aria-controls="ui-transaction">
                <span class="menu-title">Transaction Management</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-bank menu-icon"></i>
            </a>
            <div class="collapse" id="ui-transaction">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{route('new_transaction_page')}}">Add
                            Transactions</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{route('transactions')}}">Manage Transaction</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('create_bank')}}">
                <span class="menu-title">Add Banks</span>
                <i class="mdi mdi-chart-bar menu-icon"></i>
            </a>
        </li>

        @if (Auth::guard('admin')->user()->role == '1')
        <li class="nav-item">
            <a class="nav-link" href="{{route('create_admin')}}">
                <span class="menu-title">Create Admin Account</span>
                <i class="mdi mdi-chart-bar menu-icon"></i>
            </a>
        </li>
        @endif


        <li class="nav-item">
            <a class="nav-link" href="{{route('pin_page')}}">
                <span class="menu-title">Generate Pin</span>
                <i class="mdi mdi-chart-bar menu-icon"></i>
            </a>
        </li>

    </ul>
    @elseif (Auth::check())
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">

                <div class="nav-profile-image">
                    <img src="{{ asset('public/storage/profile_images')}}/{{Auth::user()->image->image_location}}" alt="profile">
                    <span class="login-status online"></span>
                </div>
                <div class="nav-profile-text d-flex flex-column">
                    <span class="font-weight-bold mb-2">{{Auth::user()->firstname}},
                        {{Auth::user()->lastname}} </span>

                </div>

                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('user_home')}}">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('transfer')}}">
                <span class="menu-title">Transfer</span>
                <i class="mdi mdi-bank menu-icon"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#popupmodal">
                <span class="menu-title">Bill Pay</span>
                <i class="mdi mdi-chip menu-icon"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#popupmodal">
                <span class="menu-title">Messages</span>
                <i class="mdi mdi-message-text menu-icon"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#popupmodal">
                <span class="menu-title">Special Offers</span>
                <i class="mdi mdi-trophy-award menu-icon"></i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#popupmodal">
                <span class="menu-title">Open an Account</span>
                <i class="mdi mdi-book-open-page-variant menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#popupmodal">
                <span class="menu-title">Alert</span>
                <i class="mdi mdi-alert-box menu-icon"></i>
            </a>
        </li>


    </ul>
    @endif

</nav>
