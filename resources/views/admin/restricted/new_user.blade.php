@extends('template.master')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Create a Client User</h4>
            <form class="forms-sample" method="POST" enctype="multipart/form-data" autocomplete="off" action="{{route('create_user_action')}}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">First-Name</label>
                        <input type="text" name="firstname" class="form-control" value="{{old('firstname')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">Last-Name</label>
                        <input type="text" name="lastname" class="form-control" value="{{old('lastname')}}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Email address</label>
                        <input type="email" name="email" class="form-control" value="{{old('email')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputPassword4">Password</label>
                        <input type="password" name="password" class="form-control" value="{{old('password')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Currency</label>
                        <select class="form-control" name="currency_id" required>
                            <option value="">Select a Currency</option>
                                @foreach ($currencies as $currency)
                                    <option @if( old('currency_id') == $currency->id ) selected @endif value="{{$currency->id}}">{{$currency->name}} - {{$currency->symbol}}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Transaction Status</label>
                        <select class="form-control" required name="status_id">
                            <option value="">Select a Transaction Status</option>
                            @foreach ($statuses as $status)
                                <option @if( old('status_id') == $status->id ) selected @endif value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label>Profile Image</label>
                    <input type="file" name="profile_image" class="file-upload-default">
                    <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Choose Profile Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-gradient-primary" type="button">Choose Profile Image</button>
                        </span>
                    </div>
                </div>

                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <button type="reset" class="btn btn-light">Reset</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('other_scripts')
<script src="{{asset('assets/js/file-upload.js')}}"></script>
@endsection
