@extends('template.master')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Allocate Accounts to Clients</h4>
            <form class="forms-sample" method="POST" autocomplete="off" action="{{route('allocate_accounts_action')}}" >
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">User</label>
                        <select class="form-control" name="user_id" required>
                            <option value="">Select a User</option>
                                @foreach ($users as $user)
                                    <option @if( old('user_id') == $user->id ) selected @endif value="{{$user->id}}">{{$user->firstname}} {{$user->lastname}} - {{$user->email}}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Account Type</label>
                        <select class="form-control" required name="account_type_id">
                            <option value="">Select an Account Type</option>
                            @foreach ($account_types as $account_type)
                                <option @if( old('account_type_id') == $account_type->id ) selected @endif value="{{$account_type->id}}">{{$account_type->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Bank </label>
                        <select class="form-control" required name="bank_id">
                            <option value="">Select an Bank</option>
                            @foreach ($banks as $bank)
                                <option @if( old('bank_id') == $bank->id ) selected @endif value="{{$bank->id}}">{{$bank->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">Account Number</label>
                        <input type="number" name="account_number" class="form-control" value="{{old('account_number')}}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Account Balance</label>
                        <input type="number" name="balance" class="form-control" value="{{old('balance')}}" required>
                    </div>


                </div>

                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <button type="reset" class="btn btn-light">Reset</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('other_scripts')
<script src="{{asset('assets/js/file-upload.js')}}"></script>
@endsection
