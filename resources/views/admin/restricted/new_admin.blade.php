@extends('template.master')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Create an Admin User</h4>
            <form class="forms-sample" method="POST" autocomplete="off" action="{{route('create_admin_action')}}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">First-Name</label>
                        <input type="text" name="firstname" class="form-control" value="{{old('firstname')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">Last-Name</label>
                        <input type="text" name="lastname" class="form-control" value="{{old('lastname')}}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Email address</label>
                        <input type="email" name="email" class="form-control" value="{{old('email')}}" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputPassword4">Password</label>
                        <input type="password" name="password" class="form-control" value="{{old('password')}}" required>
                    </div>

                </div>

                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <button type="reset" class="btn btn-light">Reset</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('other_scripts')
<script src="{{asset('assets/js/file-upload.js')}}"></script>
@endsection
