@extends('template.master')

@section('content')

<div class="col-lg-4 grid-margin stretch-card">
    <div class="card table-responsive">
        <div class="card-body">
            <h4 class="card-title">Pins</h4>
            <form action="{{route('create_pin')}}" method="POST">
                {{ csrf_field() }}
                <button class="btn btn-gradient-info btn-sm" type="submit">Create Pin</button>
            </form>
            <table class="table table-hover">
                <thead>
                    <tr style="color:#1788ff;">
                        <th>#</th>
                        <th> Pin </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0;?>
                    @foreach ($pins as $pin)
                    <tr>
                        <td>{{$i+=1}}</td>
                        <td>{{$pin->pin}}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection
