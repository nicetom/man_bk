@extends('template.master')

@section('content')

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card table-responsive">
        <div class="card-body">
            <h4 class="card-title">Users</h4>

            <table class="table table-hover">
                <thead>
                    <tr style="color:#1788ff;">
                        <th>#</th>
                        <th> Image </th>
                        <th> Full name </th>
                        <th> Email </th>
                        <th> Currency </th>
                        <th> Status </th>
                        <th> Allocated Accounts</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0;?>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{$i+=1}}</td>
                        <td class="py-1">
                            <img src="{{ asset('public/storage/profile_images') }}/{{$user->image->image_location}}" alt="image">
                        </td>
                        <td> <span id="firstname-{{$user->id}}">{{$user->firstname}}</span> <span id="lastname-{{$user->id}}">{{$user->lastname}}</span> </td>
                        <td id="email-{{$user->id}}"> {{$user->email}}</td>
                        <td> <span style="display:none;" id="currency-{{$user->id}}">{{$user->currency->id}}</span> {{$user->currency->symbol}} - {{$user->currency->name}} </td>
                        <td><span style="display:none;" id="status-{{$user->id}}">{{$user->status->id}}</span> <label class="{{$user->status->css_class}}">{{$user->status->name}}</label> </td>
                        <td> {{$user->accounts->count() }} </td>
                        <td>
                            <button data-toggle="modal" id="{{$user->id}}" data-target="#editModal" class="btn btn-gradient-primary btn-sm edit">Edit</button>
                            <button data-toggle="modal" id="{{$user->id}}" data-target="#exampleModal" class="btn btn-gradient-danger btn-sm delete">Delete</button>
                        </td>
                    </tr>
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteLabel">Delete</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form action="{{route('delete_user')}}" method="POST">
              <input type="hidden" name="user_id" required>
            <button type="submit" class="btn btn-danger">Delete</button>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('update_normal_user')}}" autocomplete="off" method="POST" id="edit_form">
                {{ csrf_field() }}
                <div class="form-row">

                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">Firstname</label>
                        <input type="text" name="firstname" class="form-control" required>
                    </div>
                    <input type="hidden" name="user_id">
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Lastname</label>
                        <input type="text" name="lastname" class="form-control" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">Email</label>
                        <input type="email" name="email" class="form-control" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Currency</label>
                        <select id="currency" name="currency_id" class="form-control" required>
                            @foreach ($currencies as $currency)
                            <option value="{{$currency->id}}">{{$currency->name}} - {{$currency->symbol}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Status</label>
                        <select id="status" type="text" name="status_id" class="form-control" required>
                            @foreach ($statuses as $status)
                            <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">New Password (Optional)</label>
                        <input type="password" name="password" class="form-control">
                    </div>


                </div>
            </form>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" form="edit_form" class="btn btn-success">Update</button>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('other_scripts')
<script type="text/javascript">
    $('.delete').click(function(e){
        var firstname = $('#firstname-'+this.id).text();
        var lastname = $('#lastname-'+this.id).text();
        $('#deleteLabel').text('Delete '+firstname+' '+lastname);
        $('input[name="user_id"]').val(this.id);
    });

    $('.edit').click(function(e){
        var firstname = $('#firstname-'+this.id).text();
        var lastname = $('#lastname-'+this.id).text();
        var email = $('#email-'+this.id).text();
        var currency = $('#currency-'+this.id).text();
        var status = $('#status-'+this.id).text();
        $('input[name="firstname"]').val(firstname);
        $('input[name="lastname"]').val(lastname);
        $('input[name="email"]').val(email);
        $('input[name="user_id"]').val(this.id);

        $('#currency option:selected').removeAttr('selected');
        $('#status option:selected').removeAttr('selected');

        $('#currency option[value="'+currency+'"]').attr("selected", "selected");
        $('#status option[value="'+status+'"]').attr("selected", "selected");
    });
</script>
@endsection
