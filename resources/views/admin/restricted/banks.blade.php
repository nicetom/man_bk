@extends('template.master')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Create a Bank</h4>
            <form class="forms-sample" method="POST" enctype="multipart/form-data" autocomplete="off" action="{{route('create_bank_action')}}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="exampleInputName1">Bank-Name</label>
                        <input type="text" name="name" class="form-control" value="{{old('name')}}" required>
                    </div>

                </div>

                <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <button type="reset" class="btn btn-light">Reset</button>
            </form>
        </div>
    </div>
</div>

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card table-responsive">
        <div class="card-body">
            <h4 class="card-title">Banks</h4>

            <table class="table table-hover">
                <thead>
                    <tr style="color:#1788ff;">
                        <th>#</th>
                        <th> Name </th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0;?>
                    @foreach ($banks as $bank)
                    <tr>
                        <td>{{$i+=1}}</td>
                        <td id="name-{{$bank->id}}">{{$bank->name}}</td>
                        <td>
                            <button data-toggle="modal" id="{{$bank->id}}" data-target="#editModal" class="btn btn-gradient-primary btn-sm edit">Edit</button>
                            <button data-toggle="modal" id="{{$bank->id}}" data-target="#exampleModal" class="btn btn-gradient-danger btn-sm delete">Delete</button>
                        </td>
                    </tr>
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form action="{{route('delete_bank')}}" method="POST">
              <input type="hidden" name="bank_id" required>
            <button type="submit" class="btn btn-danger">Delete</button>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('edit_bank_action')}}" autocomplete="off" method="POST" id="edit_form">
                {{ csrf_field() }}
                <div class="form-row">

                    <div class="form-group col-md-12">
                        <label for="exampleInputName1">Bank Name</label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <input type="hidden" name="bank_id">

                </div>
            </form>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            <button form="edit_form" type="submit" class="btn btn-success">Update</button>

        </div>
      </div>
    </div>
  </div>
@endsection


@section('other_scripts')
<script type="text/javascript">
    $('.delete').click(function(e){
        var name = $('#name-'+this.id).text();
        $('#deleteLabel').text('Are you sure you want to delete '+name);
        $('input[name="bank_id"]').val(this.id);
    });

    $('.edit').click(function(e){
        var name = $('#name-'+this.id).text();
        $('input[name="name"]').val(name);
        $('input[name="bank_id"]').val(this.id);

    });
</script>
@endsection


