@extends('template.master')

@section('content')

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card table-responsive">
        <div class="card-body">
            <h4 class="card-title">Transactions</h4>

            <table class="table table-hover">
                <thead>
                    <tr style="color:#1788ff;">
                        <th>#</th>
                        <th>Date </th>
                        <th>User </th>
                        <th>Account Balance</th>
                        <th> Transaction Description</th>
                        <th> Date </th>
                        <th> Amount </th>
                        <th> Status</th>
                        <th>Transaction Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0;?>
                    @foreach ($transactions as $transaction)
                    <tr>
                        <td>{{$i+=1}}</td>
                        <td id="transaction-date-{{$transaction->id}}">{{$transaction->date_of_transaction}}</td>
                        <td>{{$transaction->account->user->firstname}} {{$transaction->account->user->lastname}}</td>
                        <td>{{$transaction->account->user->currency->symbol}} {{$transaction->account->balance}} </td>
                        <td>{{$transaction->account_name}} </td>
                        <td id="transaction-description-{{$transaction->id}}">{{$transaction->description}} </td>
                        <td>{{$transaction->date_of_transaction}} </td>
                        <td>{{ $transaction->account->user->currency->symbol }} {{$transaction->amount}} </td>
                        <td><span style="display:none" id="transaction-status-{{$transaction->id}}">{{$transaction->status->id}}</span><span class="{{$transaction->status->css_class}}">{{$transaction->status->name}}</span></td>
                        <td> <span class="{{$transaction->transaction_type->css_class}}">{{$transaction->transaction_type->name}}</span> </td>

                        <td>

                            <button data-toggle="modal" id="{{$transaction->id}}" data-target="#editModal" class="btn btn-gradient-primary btn-sm edit">Edit Status</button>

                            <button data-toggle="modal" id="{{$transaction->id}}" data-target="#exampleModal" class="btn btn-gradient-danger btn-sm delete">Delete</button>
                        </td>
                    </tr>
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteLabel">Delete this transaction</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form action="{{route('delete_transaction')}}" method="POST">
              <input type="hidden" name="transaction_id" required>
            <button type="submit" class="btn btn-danger">Delete</button>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteLabel">Edit Transaction Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('edit_transaction')}}" autocomplete="off" method="POST" id="edit_form">
                {{ csrf_field() }}
                <div class="form-row">

                    <input type="hidden" name="transaction_id">

                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail3">Status</label>
                        <select id="status" name="status_id" class="form-control" required>
                            @foreach ($statuses as $status)
                            <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail3">Description</label>
                        <input type="text" id="description" name="description" class="form-control" required>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail3">Date</label>
                        <input name="date_of_transaction" type="date" class="form-control" required>
                    </div>


                </div>
            </form>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form action="{{route('delete_transaction')}}" method="POST">
              <input type="hidden" name="transaction_id" required>
            <button type="submit" form="edit_form" class="btn btn-danger">Update</button>
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>


@endsection

@section('other_scripts')
<script type="text/javascript">
    $('.delete').click(function(e){
        $('input[name="transaction_id"]').val(this.id);
    });

    $('.edit').click(function(e){
        $('input[name="transaction_id"]').val(this.id);
        $('input[name="description"]').val($('#transaction-description-'+this.id).text());
        $('input[name="date_of_transaction"]').val($('#transaction-date-'+this.id).text());
        var status = $('#transaction-status-'+this.id).text();
        $('#status option:selected').removeAttr('selected');
        $('#status option[value="'+status+'"]').attr("selected", "selected");
    });
</script>
@endsection

