@extends('template.master')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Add Transaction Record to Clients Account</h4>
            <form class="forms-sample" method="POST" autocomplete="off" action="{{route('new_transaction_action')}}" >
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">User</label>
                        <select id="user_id" class="form-control" name="user_id" required>
                            <option value="">Select a User</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}">{{$user->firstname}} {{$user->lastname}} - {{$user->email}}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Allocated Accounts</label>
                        <select class="form-control" required id="accounts" name="account_id">

                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Transaction Type</label>
                        <select class="form-control" required name="transaction_type_id">
                            <option value="">Select an Transaction Type</option>
                            @foreach ($transaction_types as $transaction_type)
                                <option @if( old('transaction_type_id') == $transaction_type->id ) selected @endif value="{{$transaction_type->id}}">{{$transaction_type->name}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Recieving Bank </label>
                        <select class="form-control" required name="bank_id">
                            <option value="">Select a Bank</option>
                            @foreach ($banks as $bank)
                                <option @if( old('bank_id') == $bank->id ) selected @endif value="{{$bank->id}}">{{$bank->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleSelectGender">Transaction Status </label>
                        <select class="form-control" required name="status_id">
                            <option value="">Select a Status</option>
                            @foreach ($statuses as $status)
                                <option @if( old('status_id') == $status->id ) selected @endif value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputName1">Account Name</label>
                        <input type="text" name="account_name" class="form-control" value="{{old('account_name')}}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Account Number</label>
                        <input type="number" minlength="10" maxlength="12" name="account_number" class="form-control" value="{{old('account_number')}}" required>
                    </div>



                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail3">Amount</label>
                        <input type="number" name="amount" class="form-control" value="{{old('amount')}}" required>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail3">Date</label>
                        <input type="date" name="date_of_transaction" class="form-control" required>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="exampleTextarea1">Description</label>
                        <textarea class="form-control" value="{{old('description')}}" required name="description" rows="4" placeholder="Enter Description of Transaction"></textarea>
                      </div>

                </div>

                <button type="submit" id="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                <button type="reset" class="btn btn-light">Reset</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('other_scripts')
<script src="{{asset('assets/admin/js/accounts.js')}}"></script>
@endsection
