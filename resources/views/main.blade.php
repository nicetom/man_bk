@extends('includes.master')

@section('content')
<main id="content" class="background-s">

  <section class="hero hero-banner full-image-banner spacing" id="step-1">


    <img class="hidden-xs-down image aos-init aos-animate"
      src="{{url('/')}}/assets/main/SCU/media/Images/2020_Covid19_Communication_Website_1920x515_right.jpg?ext=.jpg"
      alt="" data-aos="fade-down" data-aos-duration="1000">


    <img class="hidden-xs-up image aos-init aos-animate"
      src="{{url('/')}}/assets/main/SCU/media/Images/2020_Covid19_Communication_Website_375x240.jpg?ext=.jpg" alt=""
      data-aos="fade-down" data-aos-duration="1000">


    <div class="grid-container">

      <div class="overlay-banner background-color-maroon aos-init aos-animate" data-aos="fade-up"
        data-aos-duration="1000" data-aos-delay="350">
        <div class="banner-content">
          <h3 class="title is-3 text-white">COVID-19 Updates</h3>
        </div>
        <div class="banner-content text-content">
          <p class="text-white">
          </p>
          <p class="text-white">Reduced hours for<br>
            in-branch service</p>

          <p></p>
        </div>
        <div class="banner-content">
          <a class="cta-link  fill-primary" href="#" target="_blank">
            <div class="cta-wrapper">
              <span class="cta-text">Learn More</span>
              <div class="cta-img dark-arrow"></div>
            </div>
          </a>
        </div>
      </div>
    </div>




  </section>

  <section class="section-spacing">
    <div class="contact-banner">

      <div class="double-promo-banner banner-column column-two background-color-red">

        <div class="double-promo-banner-image background-image-cover background-image center-top"
          style="background-image: url('assets/main/SCU/media/Images/2020_Covid-19_Thankyou_Web-Image-1068x400_alt.jpg?ext=.jpg')">
        </div>

        <div class="content-wrapper left">
          <!--  -->
          <h3 class="title is-3 text-white">It’s our community that will get us through times like these. </h3>
          <a class="cta-link fill-primary"
            href="#">
            <div class="cta-wrapper">
              <span class="cta-text">Share your thanks</span>
              <div class="cta-img dark-arrow"></div>
            </div>
          </a>
          <!--  -->
        </div>




      </div>


      <div class="double-promo-banner banner-column column-two background-color-maroon">

        <div class="double-promo-banner-image background-image-cover background-image center-top"
          style="background-image: url('assets/main/SCU/media/Images/2020_Deposit_CRA-Direct-Deposit_Web-1068-x-400.jpg?ext=.jpg')">
        </div>

        <div class="content-wrapper left">
          <!--  -->
          <h3 class="title is-3 text-white">Sign up for CRA Direct Deposit</h3>
          <a class="cta-link fill-primary" href="#">
            <div class="cta-wrapper">
              <span class="cta-text">Learn more</span>
              <div class="cta-img dark-arrow"></div>
            </div>
          </a>
          <!--  -->
        </div>




      </div>

    </div>
  </section>

  <section class="section-spacing">
    <div class="hero grid-container">
      <div class="hero-body text-hero no-body">

        <div class="container text-centered">
          <h2 class="title is-2 is-spaced reduced-margin text-maroon ">
            Products &amp; Services to Help You
          </h2>
        </div>

      </div>
    </div>


    <div class="grid-container">
      <div class="columns is-multiline card-columns">
        <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile" >
          <a class="card-link" href="#">
            <div class="card linking-service-card">
              <div class="card-content card-header ">
                <div class="media service-card-media linking-service-card-media">
                  <div class="media-image">
                    <img class="image visible-image"
                      src="{{url('/')}}/assets/main/SCU/media/Images/growth.svg?ext=.svg" alt="Placeholder image">
                    <img class="image hidden-image"
                      src="{{url('/')}}/assets/main/SCU/media/Images/grow-white.svg?ext=.svg"
                      alt="Placeholder image">
                  </div>

                  <div class="media-content linking-service-card-content">
                    <h4 class="title is-6 card-title">Investing</h4>
                    <div class="content-body">
                      <p>Grow your savings tax-free with a TFSA</p>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile">





          <a class="card-link" href="#">
            <div class="card linking-service-card">
              <div class="card-content card-header ">
                <div class="media service-card-media linking-service-card-media">
                  <div class="media-image">
                    <img class="image visible-image"
                      src="{{url('/')}}/assets/main/SCU/media/Images/home-dark.svg?ext=.svg"
                      alt="Placeholder image">
                    <img class="image hidden-image"
                      src="{{url('/')}}/assets/main/SCU/media/Images/home-white.svg?ext=.svg"
                      alt="Placeholder image">
                  </div>

                  <div class="media-content linking-service-card-content">
                    <h4 class="title is-6 card-title">Mortgages</h4>
                    <div class="content-body">
                      <p>Transform your space&nbsp;into your dream home with an {{config('app.name')}} Renovation Mortgage</p>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="column auto-column is-4-desktop is-6-tablet is-12-mobile" >





          <a class="card-link" href="#">
            <div class="card linking-service-card">
              <div class="card-content card-header ">
                <div class="media service-card-media linking-service-card-media">
                  <div class="media-image">
                    <img class="image visible-image"
                      src="{{url('/')}}/assets/main/SCU/media/Images/Investment-Certificates.svg?ext=.svg"
                      alt="Placeholder image">
                    <img class="image hidden-image"
                      src="{{url('/')}}/assets/main/SCU/media/Images/SCU_ProductIcon_48-reverse_1.png?ext=.png"
                      alt="Placeholder image">
                  </div>

                  <div class="media-content linking-service-card-content">
                    <h4 class="title is-6 card-title">Our Deposit Guarantee</h4>
                    <div class="content-body">
                      <p>All deposits are guaranteed 100% by the Deposit Guarantee Corporation of Manitoba.</p>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="section-spacing hero background-color-maroon">
    <div class="hero-body grid-container rate-banner-wrapper">
      <h4 class="title is-4 text-centered text-white">Featured Rates
      </h4>
      <div class="rates-banner">

      </div>

      <div class="rates-banner">
        <div class="rates-column">
          <p class="rate">1.30<sup>%<span>*</span></sup></p>
          <p>Regular Savings</p>

        </div>
        <div class="rates-column">
          <p class="rate">1.95<sup>%<span>*</span></sup></p>
          <p>Closed Variable (APR†)</p>

        </div>
        <div class="rates-column">
          <p class="rate">2.00<sup>%<span>*</span></sup></p>
          <p>13-month GIC special</p>

        </div>

      </div>
      <div class="rates-cta">
        <a class="cta-link fill-primary" href="#">
          <div class="cta-wrapper">
            <span class="cta-text">See All Rates</span>
            <div class="cta-img dark-arrow"></div>
          </div>
        </a>
        <p class="legal-text text-white text-centered">
          *Rates subject to change. Current as of April 25, 2020
        </p>
      </div>
    </div>
  </section>


  <section class="promo-banner background-color-red">

    <div class="promo-image center-top hidden-xs-down"
      style="background-image: url({{url('assets/main/SCU/media/Images/2020_04_Member-Referral_Spring_Web_1068x400.jpg?ext=.jpg')}})">
    </div>



    <div class="promo-image background-image center-top hidden-xs-up"
      style="background-image: url({{url('assets/main/SCU/media/Images/2020_04_Member-Referral_Spring_Web_380x530.jpg?ext=.jpg')}})">
    </div>


    <div class="promo-content contactus-banner">
      <h3 class="title is-3 white">Refer a new member and you could win $500</h3>
      <p class="promo-p hidden-lg-down white">
        Refer a friend or family member to {{config('app.name')}} and be entered into a draw to win 1 of 6 $500 savings accounts.
      </p>

      <a class="cta-link fill-primary" href="#" target="_blank">
        <div class="cta-wrapper">
          <span class="cta-text ">Learn More</span>
          <div class="cta-img dark-arrow">
          </div>
        </div>
      </a>

    </div>
  </section>

</main>
@endsection
