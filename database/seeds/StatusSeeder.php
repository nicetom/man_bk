<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'name' => 'Success',
                'css_class' => 'badge badge-success'
            ],
            [
                'name' => 'Declined',
                'css_class' => 'badge badge-danger'
            ],
            [
                'name' => 'Pending',
                'css_class' => 'badge badge-warning'
            ]
        ];

            foreach($statuses as $status){
                Status::create([
                    'name' => $status['name'],
                    'css_class' => $status['css_class']
                ]);
            }
    }
}
