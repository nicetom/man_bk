<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;
class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                'name' => 'Dollar',
                'symbol' => '$'
            ],
            [
                'name' => 'Euro',
                'symbol' => '€'
            ],
            [
                'name' => 'Pounds Sterling',
                'symbol' => '£'
            ]
        ];
        foreach($currencies as $currency){
            Currency::create([
                'name' => $currency['name'],
                'symbol' => $currency['symbol']
            ]);
        }
    }
}
