<?php

use Illuminate\Database\Seeder;
use App\Models\TransactionTypes;

class TransactionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name' => 'Credit',
                'css_class' => 'money-credit'
            ],
            [
                'name' => 'Debit',
                'css_class' => 'money-debit'
            ],
        ];

            foreach($types as $type){
                TransactionTypes::create([
                    'name' => $type['name'],
                    'css_class' => $type['css_class']
                ]);
            }
    }
}
