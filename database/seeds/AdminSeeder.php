<?php

use Illuminate\Database\Seeder;
use App\Models\Admins;
use Illuminate\Support\Facades\Hash;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admins::create([
            'firstname' => 'Peter',
            'lastname' => 'Vicos',
            'email' => 'admin@admin.com',
            'role' => '1',
            'password' => Hash::make('polinium')
        ]);
    }
}
