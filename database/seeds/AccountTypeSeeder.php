<?php

use Illuminate\Database\Seeder;
use App\Models\AccountType;
class AccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Savings','Checking','Investment','Current','Deposit','Business'
        ];

        foreach ($types as $type) {
            AccountType::create([
                'name' => $type
            ]);
         }
    }
}
