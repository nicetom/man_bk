<?php

use Illuminate\Database\Seeder;
use App\Models\Banks;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            'JP Morgan Chase Bank', 'Bank of America', 'Wells Fargo', 'Citigroup', 'Goldman Sachs', 'Morgan Stanely', 'U.S. Bankcorp',
            'PNC Financial Services', 'TD Bank', 'Capital One Bank', 'HSBC', 'Bank of the West', 'State Street Corporation', 'Sun Trust Bank',
            'American Express', 'Ally Financial', 'Barclays', 'USAA', 'Citizen Financial Group', 'Fifth Third Bank'
        ];

        foreach ($banks as $bank) {
            Banks::create([
                'name' => $bank
            ]);
         }
    }
}
