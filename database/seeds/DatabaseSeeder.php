<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(TransactionTypesSeeder::class);
        $this->call(AccountTypeSeeder::class);
    }
}
