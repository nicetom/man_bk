<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home')->name('home_page');
Route::get('personal/{name}','PagesController@templateFunction')->name('template');
Route::get('login', 'PagesController@login')->name('login_page');
Route::post('login', 'LoadController@login')->name('user_login_action');


Route::get('profile_image/{user_id}','LoadController@getImage')->name('profile_image');
Route::get('transactions/{account_id}','User\LoadController@getTransactions');

Route::group(['prefix' => 'user','namespace' => 'User','middleware' => 'user'], function () {
    Route::get('/','PagesController@homePage')->name('user_home');
    Route::get('verification','PagesController@verification')->name('verification');
    Route::post('verification','LoadController@verification')->name('verification_action');
    Route::get('logout','LoadController@logout')->name('logout');
    Route::get('transfer','PagesController@transferPage')->name('transfer');
    Route::post('transfer','LoadController@transfer')->name('transfer_action');
    Route::get('transactions/{account_id}','LoadController@getTransactions');

});

Route::group(['prefix' => 'admin','namespace' => 'Admin'], function () {
   Route::get('/','PagesController@login')->name('admin_login');
   Route::post('login','LoadController@authenticate')->name('login_action');

    Route::group(['prefix' => 'dashboard','middleware' => 'admin'], function () {
       Route::get('/','PagesController@dashboard')->name('admin_home');
       Route::get('logout','LoadController@logout')->name('admin_logout');
       Route::get('create_user','PagesController@createUser')->name('create_user');
       Route::post('create_user','LoadController@createUser')->name('create_user_action');
       Route::get('allocate_accounts','PagesController@allocateAccount')->name('allocate_accounts');
       Route::post('allocate_accounts','LoadController@allocateAccount')->name('allocate_accounts_action');
       Route::get('add_transaction','PagesController@newTransaction')->name('new_transaction_page');
       Route::post('add_transaction','LoadController@newTransaction')->name('new_transaction_action');
       Route::get('accounts/{user_id}','LoadController@getAccounts');
       Route::get('users','PagesController@users')->name('users');
       Route::post('update','LoadController@updateUser')->name('update_normal_user');
       Route::post('delete','LoadController@deleteUser')->name('delete_user');
       Route::get('pins','PagesController@pinPage')->name('pin_page');
       Route::post('create_pin','LoadController@createPin')->name('create_pin');
       Route::get('create_bank','PagesController@createBank')->name('create_bank');
       Route::get('create_admin','PagesController@createAdmin')->name('create_admin');
       Route::post('create_admin','LoadController@createAdmin')->name('create_admin_action');
       Route::post('create_bank','LoadController@createBank')->name('create_bank_action');
       Route::post('edit_bank','LoadController@updateBank')->name('edit_bank_action');
       Route::post('delete_bank','LoadController@deleteBank')->name('delete_bank');
       Route::get('transactions','PagesController@transactionPage')->name('transactions');
       Route::post('delete_transaction','LoadController@deleteTransaction')->name('delete_transaction');
       Route::post('edit_transaction','LoadController@editTransaction')->name('edit_transaction');
    });
});
